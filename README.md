![roverslist2.PNG](https://bitbucket.org/repo/kbd4nq/images/3447394931-roverslist2.PNG)![roverslist3.PNG](https://bitbucket.org/repo/kbd4nq/images/3919828563-roverslist3.PNG)![roverslist1.PNG](https://bitbucket.org/repo/kbd4nq/images/3434539593-roverslist1.PNG)Rover’s List is a social media site for dog enthusiasts.
 
Rover’s List is an ASP.NET Web API, MVC, and AngularJS Application.
 
The Dependency Injection software design pattern was implemented in all application data controllers and data services.
 
An Entity Framework Code First approached was used to implement the SQL Server Database.
 
The Rover’s List database contains eight (8) many-to-many relationships.
 
Two (2) software development languages were used. C# and TypeScript (also known as ECMAScript 2015), along with Visual Studio's built in translation of TypeScript into JavaScript.
 
Web pages were developed using HTML5, CSS3, and AngularJS for two-way data binding and client-side validation.
 
Angular UI Bootstrap was used for the Web page layout and controls.
 
Google Maps and uploading images to cloud storage are some of the advanced techniques used in the Application.
 
Currently, there are three (4) types of Rover’s List Users, a Guest, a Registered User without a User Profile, a Registered User with a User Profile, and a Site Administrator.