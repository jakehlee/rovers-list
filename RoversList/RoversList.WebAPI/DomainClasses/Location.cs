﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.WebAPI.DomainClasses
{
    public class Location
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string PostalCode { get; set; }

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool Inactive { get; set; }

        public ICollection<Board> Boards { get; set; }

    }
}