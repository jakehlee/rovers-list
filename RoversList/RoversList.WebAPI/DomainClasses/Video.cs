﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.WebAPI.DomainClasses
{
    public class Video
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool Inactive { get; set; }

        public int ClickCount { get; set; }

    }
}