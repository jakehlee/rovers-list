﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoversList.WebAPI.DomainClasses
{
    public class PhotoFlag
    {
        public int Id { get; set; }

        public int PhotoId { get; set; }
        [ForeignKey("PhotoId")]
        public Photo Photo { get; set; }

        public int UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public UserProfile UserProfile { get; set; }

        public string FlagText { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Inactive { get; set; }

    }
}