﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoversList.WebAPI.DomainClasses
{
    public class BoardFlag
    {
        public int Id { get; set; }

        public int BoardId { get; set; }
        [ForeignKey("BoardId")]
        public Board Board { get; set; }

        public int UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public UserProfile UserProfile { get; set; }

        public string FlagText { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Inactive { get; set; }

    }
}