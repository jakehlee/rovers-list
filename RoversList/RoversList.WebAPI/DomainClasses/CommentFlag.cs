﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoversList.WebAPI.DomainClasses
{
    public class CommentFlag
    {
        public int Id { get; set; }

        public int CommentId { get; set; }
        [ForeignKey("CommentId")]
        public Comment Comment { get; set; }

        public int UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public UserProfile UserProfile { get; set; }

        public string FlagText { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Inactive { get; set; }

    }
}