﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Conventions;
using RoversList.WebAPI.DomainClasses;

namespace RoversList.WebAPI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            this.Configuration.LazyLoadingEnabled = false;
        }


        public IDbSet<ActivityLog> ActivityLogs { get; set; }
        public IDbSet<Ad> Ads { get; set; }
        public IDbSet<Board> Boards { get; set; }        
        public IDbSet<BoardFlag> BoardFlags { get; set; }
        public IDbSet<BoardType> BoardTypes { get; set; }
        public IDbSet<Comment> Comments { get; set; }
        public IDbSet<CommentFlag> CommentFlags { get; set; }
        public IDbSet<Location> Locations { get; set; }
        public IDbSet<Photo> Photos { get; set; }
        public IDbSet<PhotoFlag> PhotoFlags { get; set; }
        public IDbSet<Review> Reviews { get; set; }
        public IDbSet<ReviewFlag> ReviewFlags { get; set; }
        public IDbSet<State> States { get; set; }
        public IDbSet<UserProfile> UserProfiles { get; set; }
        public IDbSet<UserTag> UserTags { get; set; }
        public IDbSet<Vendor> Vendors { get; set; }
        public IDbSet<Video> Videos { get; set; }


        // RDG:  http://csharpcode.org/entity-framework-fix-foreign-key-constraint-may-cause-cycles-or-multiple-cascade-paths/
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Entity<Location>().Property(p => p.Latitude).HasPrecision(18, 9);
            modelBuilder.Entity<Location>().Property(p => p.Longitude).HasPrecision(18, 9);
            base.OnModelCreating(modelBuilder);
        }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}