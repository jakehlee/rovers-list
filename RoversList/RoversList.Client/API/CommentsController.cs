﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;
using RoversList.Client.DTO;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/Comments")]
    public class CommentsController : ApiController {
        private ICommentsService _commentsService;

        public CommentsController(ICommentsService commentsService) {
            _commentsService = commentsService;
        }

        // GET: api/Comments
        //[Route("")]
        public IEnumerable<Comment> Get() {
            return _commentsService.ListComments();
        }

        // GET: api/Comments/5
        public Comment Get(int id) {
            return _commentsService.FindComment(id);
        }

        // POST: api/Comments
        public Comment Post(CommentAPI comment) {
            if (comment.Id == 0) {
                return _commentsService.CreateComment(comment);
            }
            else {
                return _commentsService.EditComment(comment);
            }
        }

        // DELETE: api/Comments/5
        public void Delete(int id) {
            _commentsService.DeleteComment(id);
        }
    }

}