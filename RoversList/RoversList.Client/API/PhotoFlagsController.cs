﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/PhotoFlags")]
    public class PhotoFlagsController : ApiController {
        private IPhotoFlagsService _photoFlagsService;

        public PhotoFlagsController(IPhotoFlagsService photoFlagsService) {
            _photoFlagsService = photoFlagsService;
        }

        // GET: api/PhotoFlags
        //[Route("")]
        public IEnumerable<PhotoFlag> Get() {
            return _photoFlagsService.ListPhotoFlags();
        }
        // GET: api/PhotoFlags

        public PhotoFlag Get(int id) {
            return _photoFlagsService.FindPhotoFlags(id);
        }

        // POST: api/PhotoFlags
        public PhotoFlag Post(PhotoFlag photoFlags) {
            if (photoFlags.Id == 0) {
                return _photoFlagsService.CreatePhotoFlag(photoFlags);
            }
            else {
                return _photoFlagsService.EditPhotoFlag(photoFlags);
            }
        }

        // DELETE: api/PhotoFlags/5
        public void Delete(int id) {
            _photoFlagsService.DeletePhotoFlag(id);
        }
    }

}