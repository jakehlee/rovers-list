﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/Videos")]
    public class VideosController : ApiController {
        private IVideosService _videosService;

        public VideosController(IVideosService videosService) {
            _videosService = videosService;
        }

        // GET: api/Videos
        //[Route("")]
        public IEnumerable<Video> Get() {
            return _videosService.ListVideos();
        }
        // GET: api/Videos

        public Video Get(int id) {
            return _videosService.FindVideo(id);
        }

        // POST: api/Videos
        public Video Post(Video video) {
            if (video.Id == 0) {
                return _videosService.CreateVideo(video);
            }
            else {
                return _videosService.EditVideo(video);
            }
        }

        // DELETE: api/Videos/5
        public void Delete(int id) {
            _videosService.DeleteVideo(id);
        }
    }

}