﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/Vendors")]
    public class VendorsController : ApiController {
        private IVendorsService _vendorsService;

        public VendorsController(IVendorsService vendorsService) {
            _vendorsService = vendorsService;
        }

        // GET: api/Vendors
        //[Route("")]
        public IEnumerable<Vendor> Get() {
            return _vendorsService.ListVendors();
        }
        // GET: api/Vendors

        public Vendor Get(int id) {
            return _vendorsService.FindVendor(id);
        }

        // POST: api/Vendors
        public Vendor Post(Vendor vendor) {
            if (vendor.Id == 0) {
                return _vendorsService.CreateVendor(vendor);
            }
            else {
                return _vendorsService.EditVendor(vendor);
            }
        }

        // DELETE: api/Vendors/5
        public void Delete(int id) {
            _vendorsService.DeleteVendor(id);
        }
    }

}