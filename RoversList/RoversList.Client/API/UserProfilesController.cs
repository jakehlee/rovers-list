﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers 
{
        [RoutePrefix("api/UserProfiles")]
        public class UserProfilesController : ApiController 
        {
            private IUserProfilesService _userProfilesService;

            public UserProfilesController(IUserProfilesService userProfilesService) 
            {
                _userProfilesService = userProfilesService;
            }

            // GET: api/UserProfiles
            [Route("")]
            public IEnumerable<UserProfile> Get() 
            {
                return _userProfilesService.ListUserProfiles();
            }

            // GET: api/UserProfiles/5
            [Route("{id:int}")]
            public UserProfile Get(int id)
            {
                return _userProfilesService.FindUserProfile(id);
            }

            // GET: api/UserProfiles/userId Guid
            [Route("{userId:guid}")]
            public UserProfile Get(Guid userId)
            {
                return _userProfilesService.FindUserProfileByUserId(userId);
            }

            // GET: api/UserProfiles/screenName string
            [Route("{screenName}")]
            public UserProfile Get(string screenName)
            {
                return _userProfilesService.FindUserProfileByScreenName(screenName);
            }

            // POST: api/UserProfiles
            public UserProfile Post(UserProfile userProfile) 
            {
                if (userProfile.Id == 0) 
                {
                    return _userProfilesService.CreateUserProfile(userProfile);
                }
                else 
                {
                   return  _userProfilesService.EditUserProfile(userProfile);
                }
            }

            // DELETE: api/UserProfiles/5
            public void Delete(int id) 
            {
                _userProfilesService.DeleteUserProfile(id);
            }
        }
}