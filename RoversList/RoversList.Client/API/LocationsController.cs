﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/Locations")]
    public class LocationsController : ApiController {
        private ILocationsService _locationsService;

        public LocationsController(ILocationsService locationsService) {
            _locationsService = locationsService;
        }

        // GET: api/Locations
        //[Route("")]
        public IEnumerable<Location> Get() {
            return _locationsService.ListLocations();
        }
        // GET: api/Locations

        public Location Get(int id) {
            return _locationsService.FindLocation(id);
        }

        // POST: api/Locations
        public Location Post(Location location) {
            if (location.Id == 0) {
                return _locationsService.CreateLocation(location);
            }
            else {
                return _locationsService.EditLocation(location);
            }
        }

        // DELETE: api/Locations/5
        public void Delete(int id) {
            _locationsService.DeleteLocation(id);
        }
    }

}