﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;
using RoversList.Client.DTO;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/Photos")]
    public class PhotosController : ApiController {
        private IPhotosService _photosService;

        public PhotosController(IPhotosService photosService) {
            _photosService = photosService;
        }

        // GET: api/Photos
        //[Route("")]
        public IEnumerable<Photo> Get() {
            return _photosService.ListPhotos();
        }

        // GET: api/Photos/5
        public Photo Get(int id) {
            return _photosService.FindPhoto(id);
        }

        // POST: api/Photos
        public Photo Post(PhotoAPI photo) {
            if (photo.Id == 0) {
                return _photosService.CreatePhoto(photo);
            }
            else {
                return _photosService.EditPhoto(photo);
            }
        }

        // DELETE: api/Photos/5
        public void Delete(int id) {
            _photosService.DeletePhoto(id);
        }
    }

}