﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;
using RoversList.Client.DTO;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/Reviews")]
    public class ReviewsController : ApiController {
        private IReviewsService _reviewsService;

        public ReviewsController(IReviewsService reviewsService) {
            _reviewsService = reviewsService;
        }

        // GET: api/Reviews
        //[Route("")]
        public IEnumerable<Review> Get() {
            return _reviewsService.ListReviews();
        }

        // GET: api/Reviews/5
        public Review Get(int id) {
            return _reviewsService.FindReview(id);
        }

        // POST: api/Reviews
        public Review Post(ReviewAPI review) {
            if (review.Id == 0) {
                return _reviewsService.CreateReview(review);
            }
            else {
                return _reviewsService.EditReview(review);
            }
        }

        // DELETE: api/Reviews/5
        public void Delete(int id) {
            _reviewsService.DeleteReview(id);
        }
    }

}