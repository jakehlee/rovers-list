﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;
using RoversList.Client.DTO;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/Boards")]
    public class BoardsController : ApiController {
        private IBoardsService _boardsService;

        public BoardsController(IBoardsService boardsService) {
            _boardsService = boardsService;
        }

        // GET: api/Boards
        //[Route("")]
        public IEnumerable<BoardDTO> Get() {
            return _boardsService.ListBoards();
        }

        // GET: api/Boards/5
        public BoardDetailDTO Get(int id) {
            return _boardsService.FindBoard(id);
        }

        // POST: api/Boards
        public Board Post(BoardAPI board) {
            if (board.Id == 0) {
                return _boardsService.CreateBoard(board);
            }
            else {
               return _boardsService.EditBoard(board);
            }
        }

        // DELETE: api/Boards/5
        public void Delete(int id) {
            _boardsService.DeleteBoard(id);
        }
    }

}