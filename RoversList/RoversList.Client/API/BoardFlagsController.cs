﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/BoardFlags")]
    public class BoardFlagsController : ApiController {
        private IBoardFlagsService _boardFlagsService;

        public BoardFlagsController(IBoardFlagsService boardFlagsService) {
            _boardFlagsService = boardFlagsService;
        }

        // GET: api/BoardFlags
        //[Route("")]
        public IEnumerable<BoardFlag> Get() {
            return _boardFlagsService.ListBoardFlags();
        }
        // GET: api/BoardFlags

        public BoardFlag Get(int id) {
            return _boardFlagsService.FindBoardFlag(id);
        }

        // POST: api/BoardFlags
        public BoardFlag Post(BoardFlag boardFlag) {
            if (boardFlag.Id == 0) {
                return _boardFlagsService.CreateBoardFlag(boardFlag);
            }
            else {
                return _boardFlagsService.EditBoardFlag(boardFlag);
            }
        }

        // DELETE: api/BoardFlags/5
        public void Delete(int id) {
            _boardFlagsService.DeleteBoardFlag(id);
        }
    }

}