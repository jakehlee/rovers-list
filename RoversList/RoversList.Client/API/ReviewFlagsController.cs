﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/ReviewFlags")]
    public class ReviewFlagsController : ApiController {
        private IReviewFlagsService _reviewFlagsService;

        public ReviewFlagsController(IReviewFlagsService reviewFlagsService) {
            _reviewFlagsService = reviewFlagsService;
        }

        // GET: api/ReviewFlags
        //[Route("")]
        public IEnumerable<ReviewFlag> Get() {
            return _reviewFlagsService.ListReviewFlags();
        }
        // GET: api/ReviewFlags

        public ReviewFlag Get(int id) {
            return _reviewFlagsService.FindReviewFlags(id);
        }

        // POST: api/ReviewFlags
        public ReviewFlag Post(ReviewFlag reviewFlags) {
            if (reviewFlags.Id == 0) {
                return _reviewFlagsService.CreateReviewFlags(reviewFlags);
            }
            else {
                return _reviewFlagsService.EditReviewFlags(reviewFlags);
            }
        }

        // DELETE: api/ReviewFlags/5
        public void Delete(int id) {
            _reviewFlagsService.DeleteReviewFlags(id);
        }
    }

}