﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/Ads")]
    public class AdsController : ApiController {
        private IAdsService _adsService;

        public AdsController(IAdsService adsService) {
            _adsService = adsService;
        }

        // GET: api/Ads
        //[Route("")]
        public IEnumerable<Ad> Get() {
            return _adsService.ListAds();
        }
        // GET: api/Ads

        public Ad Get(int id) {
            return _adsService.FindAd(id);
        }

        // POST: api/Ads
        public Ad Post(Ad ad) {
            if (ad.Id == 0) {
                return _adsService.CreateAd(ad);
            }
            else {
                return _adsService.EditAd(ad);
            }
        }

        // DELETE: api/Ads/5
        public void Delete(int id) {
            _adsService.DeleteAd(id);
        }
    }

}