﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/CommentFlags")]
    public class CommentFlagsController : ApiController {
        private ICommentFlagsService _commentFlagsService;

        public CommentFlagsController(ICommentFlagsService commentFlagsService) {
            _commentFlagsService = commentFlagsService;
        }

        // GET: api/CommentFlags
        //[Route("")]
        public IEnumerable<CommentFlag> Get() {
            return _commentFlagsService.ListCommentFlags();
        }
        // GET: api/CommentFlags

        public CommentFlag Get(int id) {
            return _commentFlagsService.FindCommentFlags(id);
        }

        // POST: api/CommentFlags
        public CommentFlag Post(CommentFlag commentFlags) {
            if (commentFlags.Id == 0) {
                return _commentFlagsService.CreateCommentFlags(commentFlags);
            }
            else {
                return _commentFlagsService.EditCommentFlags(commentFlags);
            }
        }

        // DELETE: api/CommentFlags/5
        public void Delete(int id) {
            _commentFlagsService.DeleteCommentFlags(id);
        }
    }

}