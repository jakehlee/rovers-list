﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RoversList.Client.API
{
    public class StatesController : ApiController
    {
        private IStateService _stateService;

        public StatesController(IStateService stateService) 
        {
            _stateService = stateService;
        }

        // GET: api/States
        public IEnumerable<State> Get()
        {
            return _stateService.ListStates();
        }

        //// GET: api/States/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/States
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/States/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/States/5
        //public void Delete(int id)
        //{
        //}
    }
}
