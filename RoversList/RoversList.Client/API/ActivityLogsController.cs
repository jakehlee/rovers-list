﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/ActivityLogs")]
    public class ActivityLogsController : ApiController {
        private IActivityLogsService _activityLogsService;

        public ActivityLogsController(IActivityLogsService activityLogsService) {
            _activityLogsService = activityLogsService;
        }

        // GET: api/ActivityLogs
        //[Route("")]
        public IEnumerable<ActivityLog> Get() {
            return _activityLogsService.ListActivityLogs();
        }
        // GET: api/ActivityLogs

        public ActivityLog Get(int id) {
            return _activityLogsService.FindActivityLog(id);
        }

        // POST: api/ActivityLogs
        public ActivityLog Post(ActivityLog activityLog) {
            if (activityLog.Id == 0) {
                return _activityLogsService.CreateActivityLog(activityLog);
            }
            else {
                return _activityLogsService.EditActivityLog(activityLog);
            }
        }

        // DELETE: api/ActivityLogs/5
        public void Delete(int id) {
            _activityLogsService.DeleteActivityLog(id);
        }
    }

}