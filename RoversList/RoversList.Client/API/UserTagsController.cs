﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/UserTags")]
    public class UserTagsController : ApiController {
        private IUserTagsService _userTagsService;

        public UserTagsController(IUserTagsService userTagsService) {
            _userTagsService = userTagsService;
        }

        // GET: api/UserTags
        //[Route("")]
        public IEnumerable<UserTag> Get() {
            return _userTagsService.ListUserTags();
        }
        // GET: api/UserTags

        public UserTag Get(int id) {
            return _userTagsService.FindUserTag(id);
        }

        // POST: api/UserTags
        public UserTag Post(UserTag userTag) {
            if (userTag.Id == 0) {
                return _userTagsService.CreateUserTag(userTag);
            }
            else {
                return _userTagsService.EditUserTag(userTag);
            }
        }

        // DELETE: api/UserTags/5
        public void Delete(int id) {
            _userTagsService.DeleteUserTag(id);
        }
    }

}