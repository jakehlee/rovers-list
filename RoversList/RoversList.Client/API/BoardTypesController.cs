﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoversList.Client.DomainClasses;
using RoversList.Client.Services;
using RoversList.Client.Models;

namespace RoversList.Client.Controllers
{
    //[RoutePrefix("api/BoardTypes")]
    public class BoardTypesController : ApiController {
        private IBoardTypesService _boardTypesService;

        public BoardTypesController(IBoardTypesService boardTypesService) {
            _boardTypesService = boardTypesService;
        }

        // GET: api/BoardTypes
        //[Route("")]
        public IEnumerable<BoardType> Get() {
            return _boardTypesService.ListBoardTypes();
        }
        // GET: api/BoardTypes

        public BoardType Get(int id) {
            return _boardTypesService.FindBoardType(id);
        }

        // POST: api/BoardTypes
        public BoardType Post(BoardType boardType) {
            if (boardType.Id == 0) {
                return _boardTypesService.CreateBoardType(boardType);
            }
            else {
                return _boardTypesService.EditBoardType(boardType);
            }
        }

        // DELETE: api/BoardTypes/5
        public void Delete(int id) {
            _boardTypesService.DeleteBoardType(id);
        }
    }

}