var RoversListApp;
(function (RoversListApp) {
    var Services;
    (function (Services) {
        var AccountService = (function () {
            function AccountService($http, accountServiceURL) {
                this.$http = $http;
                this.accountServiceURL = accountServiceURL;
            }
            AccountService.prototype.registerNewUser = function (newUser) {
                return this.$http.post(this.accountServiceURL + '/register', newUser);
            };
            AccountService.prototype.changePassword = function (newPassword) {
                return this.$http.post(this.accountServiceURL + '/changePassword', newPassword);
            };
            return AccountService;
        })();
        Services.AccountService = AccountService;
        angular.module('RoversListApp').service('accountService', AccountService);
    })(Services = RoversListApp.Services || (RoversListApp.Services = {}));
})(RoversListApp || (RoversListApp = {}));
//# sourceMappingURL=services.js.map