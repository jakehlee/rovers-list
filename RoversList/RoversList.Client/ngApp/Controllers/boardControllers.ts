﻿namespace RoversListApp.Controllers {

    class BoardListController {
        public boards

        public userId
        public userProfileId
        public screenName

        constructor(private boardService: RoversListApp.Services.BoardService,
            private $location: ng.ILocationService,
            private $window: ng.IWindowService) {

            // Note: userId will be null
            // if a guest or the user has not logged in.

            this.userId = this.$window.sessionStorage.getItem('userId');

            // Note: userProfileId and screenName will both be null
            // if a guest, the user has not logged in,
            // or the user has not yet created a User Profile.

            this.userProfileId = this.$window.sessionStorage.getItem('userProfileId');
            this.screenName = this.$window.sessionStorage.getItem('screenName');

            boardService.listBoards().then((result) => {
                this.boards = result.data;
                console.log(this.boards);
            });

        }
    }

    angular.module('RoversListApp').controller('BoardListController', BoardListController);


    class BoardDetailsController {
        public boardId
        public board

        public userId
        public userProfileId
        public screenName

        // controller level property for "Add Commment" portion of page:
        public commentText = ''

        // controller level properties for "Add Review" portion of page:
        public reviewText = ''
        public rating = 1

        addComment() {

            // build the CommentAPI to be passed
            let newComment = {
                id: 0,
                boardId: this.boardId,
                userProfileId: this.userProfileId,
                commentText: this.commentText
            };

            this.commentService.saveComment(newComment).then((result: any) => {

                // add the returned Comment to the BoardDetailDTO
                this.board.comments.push({
                    id: result.data.id,
                    commentText: result.data.commentText,
                    createdOn: result.data.createdOn,
                    inactive: result.data.inactive,
                    commentFlagCount: result.data.commentFlagCount,
                    boardId: result.data.boardId,
                    userProfileId: result.data.userProfileId,
                    screenName: this.screenName // not in the returned Comment
                });                
            });

            // reset the controller level property for "Add Commment"
            this.commentText = '';
        }

        addReview() {

            // build the ReviewAPI to be passed
            let newReview = {
                id: 0,
                boardId: this.boardId,
                userProfileId: this.userProfileId,
                reviewText: this.reviewText,
                rating: this.rating
            };

            this.reviewService.saveReview(newReview).then((result: any) => {

                // add the returned Review to the BoardDetailDTO
                this.board.reviews.push({
                    id: result.data.id,
                    reviewText: result.data.reviewText,
                    rating: result.data.rating,
                    createdOn: result.data.createdOn,
                    inactive: result.data.inactive,
                    reviewFlagCount: result.data.reviewFlagCount,
                    boardId: result.data.boardId,
                    userProfileId: result.data.userProfileId,
                    screenName: this.screenName // not in the returned Photo
                });
            });
                
            // reset the controller level properties for "Add Review"
            this.reviewText = '';
            this.rating = 1;
        }

        constructor(private boardService: RoversListApp.Services.BoardService,
            private commentService: RoversListApp.Services.CommentService,
            private reviewService: RoversListApp.Services.ReviewService,
            private $routeParams: ng.route.IRouteParamsService,
            private $window: ng.IWindowService) {

            // Note: userId will be null
            // if a guest or the user has not logged in.

            this.userId = this.$window.sessionStorage.getItem('userId');

            // Note: userProfileId and screenName will both be null
            // if a guest, the user has not logged in,
            // or the user has not yet created a User Profile.

            this.userProfileId = this.$window.sessionStorage.getItem('userProfileId');
            this.screenName = this.$window.sessionStorage.getItem('screenName');

            this.boardId = this.$routeParams['id'];

            this.boardService.getBoard(this.boardId).then((result) => {
                this.board = result.data;
            });
        }
    }

    angular.module('RoversListApp').controller('BoardDetailsController', BoardDetailsController);

    class BoardController {

        public userId
        public userProfileId
        public screenName

        public boardId

        // controller level properties for Board page:
        public boardTypeId = 0
        public locationId = 0
        public name = ""
        public description = ""
        public onlyCreatorCanUpdateMainBoardInfo = false
        public onlyCreatorCanAddBoardPhotos = false
        public url = ""
        public photoPath = ""
        public email = ""
        public phoneNumber= ""
        public eventDate = ""
        public eventHours = ""
        public eventSponsors = ""
        public volunteerOpportunities = ""
        public inactive = false

        saveBoard() {

            // build the BoardAPI to be passed
            let board = {
                id: this.boardId,
                boardTypeId: this.boardTypeId,
                locationId: this.locationId,
                userProfileId: this.userProfileId,
                name: this.name,
                description: this.description,
                onlyCreatorCanUpdateMainBoardInfo: this.onlyCreatorCanUpdateMainBoardInfo,
                onlyCreatorCanAddBoardPhotos: this.onlyCreatorCanAddBoardPhotos,
                url: this.url,
                photoPath: this.photoPath,
                email: this.email,
                phoneNumber: this.phoneNumber,
                eventDate: this.eventDate,
                eventHours: this.eventHours,
                eventSponsors: this.eventSponsors,
                volunteerOpportunities: this.volunteerOpportunities,
                inactive: this.inactive,
            }

            this.boardService.saveBoard(board).then((result: any) => {
                // go to the BoardDetails page
                this.$location.path('/' + result.data.id);
            });
        }

        constructor(private boardService: RoversListApp.Services.BoardService,
                    private $routeParams: ng.route.IRouteParamsService,
                    private $window: ng.IWindowService,
                    private $location: ng.ILocationService) {

            // Note: userId will be null
            // if a guest or the user has not logged in.

            this.userId = this.$window.sessionStorage.getItem('userId');

            // Note: userProfileId and screenName will both be null
            // if a guest, the user has not logged in,
            // or the user has not yet created a User Profile.

            this.userProfileId = this.$window.sessionStorage.getItem('userProfileId');
            this.screenName = this.$window.sessionStorage.getItem('screenName');

            this.boardId = this.$routeParams['id'];

            // if this.boardId = 0, then adding a board.

            if (this.boardId > 0) {
                this.boardService.getBoard(this.boardId).then((result: any) => {
                    this.boardTypeId = result.data.boardTypeId;
                    this.locationId = result.data.locationId;
                    this.name = result.data.name;
                    this.description = result.data.description;
                    this.onlyCreatorCanUpdateMainBoardInfo = result.data.onlyCreatorCanUpdateMainBoardInfo;
                    this.onlyCreatorCanAddBoardPhotos = result.data.onlyCreatorCanAddBoardPhotos;
                    this.url = result.data.url;
                    this.photoPath = result.data.photoPath;
                    this.email = result.data.email;
                    this.phoneNumber = result.data.phoneNumber;
                    this.eventDate = result.data.eventDate;
                    this.eventHours = result.data.eventHours;
                    this.eventSponsors = result.data.eventSponsors;
                    this.volunteerOpportunities = result.data.volunteerOpportunities;
                    this.inactive = result.data.inactive;      
               });
            }
        }
    }

    angular.module('RoversListApp').controller('BoardController', BoardController);

    class PhotoController {

        public userId
        public userProfileId
        public screenName

        public boardId
        public photoId

        // controller level properties for Photo page:
        public photoPath = ''
        public description = ''
        public inactive = false

        // Note: updatePhoto is in the PhotoDetailsController

        savePhoto() {

            // build the PhotoAPI to be passed
            let photo = {
                id: this.photoId,
                boardId: this.boardId,
                userProfileId: this.userProfileId,
                photoPath: this.photoPath,
                description: this.description,
                inactive: this.inactive
            };

            this.photoService.savePhoto(photo).then((result) => {
                // return to the BoardDetails page
                this.$location.path('/' + this.boardId);
            });
                
        }

        constructor(private photoService: RoversListApp.Services.PhotoService,
            private $routeParams: ng.route.IRouteParamsService,
            private $window: ng.IWindowService,
            private $location: ng.ILocationService) {

            // Note: userId will be null
            // if a guest or the user has not logged in.

            this.userId = this.$window.sessionStorage.getItem('userId');

            // Note: userProfileId and screenName will both be null
            // if a guest, the user has not logged in,
            // or the user has not yet created a User Profile.

            this.userProfileId = this.$window.sessionStorage.getItem('userProfileId');
            this.screenName = this.$window.sessionStorage.getItem('screenName');

            this.boardId = this.$routeParams['boardId'];
            this.photoId = this.$routeParams['id'];

            // if this.photoId = 0, then adding a photo.

            if (this.photoId > 0) {
                this.photoService.getPhoto(this.photoId).then((result: any) => {
                    this.photoPath = result.data.photoPath;
                    this.description = result.data.description;
                    this.inactive = result.data.inactive;
                });
            }
        }
    }

    angular.module('RoversListApp').controller('PhotoController', PhotoController);
}