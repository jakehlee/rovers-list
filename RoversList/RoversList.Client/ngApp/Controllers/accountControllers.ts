﻿namespace RoversListApp.Controllers {

    class AccountController {

        username: string
        password: string
        loginMessage: string
        confirmPassword: string
        registerMessage: string
        oldPassword: string
        newPassword: string
        changePasswordMessage: string
        public boards
        public photos
        public randomPhotoList
        public randomStoryList
        public randomStory
        public photoBoards: any[]
        public stories: any[]
        public events: any[]
        public randomEvents: any[]
        public randomAds: any[]
        public randomVideos: any[]
        public userActivities: any[]
        public locations: any[]

        login() {
            let data = "grant_type=password&username=" + this.username + "&password=" + this.password;
            // RGD: add full path to compilation WevAPI localhost from /Token
            this.$http.post('/Token', data,  // DLS: Replaced 'http://MoviesWebAPIApp.azurewebsites.net/Token'
                {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success((result: any) => {
                    this.$window.sessionStorage.setItem('token', result.access_token);

                    // DLS: Added custom authentication properties.
                    this.$window.sessionStorage.setItem('userName', result.userName);
                    this.$window.sessionStorage.setItem('userId', result.userId);
                    this.$window.sessionStorage.setItem('roles', result.roles);

                    let userId = result.userId;

                // DLS: Get the associated UserProfile, if it has been created.
                    this.userProfileService.getUserProfileByUserId(userId)
                        .then((up: any) => {
                            
                            if (up.data != null) {
                                // User has created his/her Profile.
                                this.$window.sessionStorage.setItem('userProfileId', up.data.id);
                                this.$window.sessionStorage.setItem('screenName', up.data.screenName);
                          }       
                    });

                    this.$location.path('/');
                }).error(() => {
                    this.loginMessage = 'Invalid user name/password';
                });
        }

        logout() {
            this.$window.sessionStorage.removeItem('token');

            // DLS: Added the removal of the custom authentication properties
            this.$window.sessionStorage.removeItem('userName');
            this.$window.sessionStorage.removeItem('userId');
            this.$window.sessionStorage.removeItem('roles');

            // DLS: Added the removal of the UserProfile properties
            this.$window.sessionStorage.removeItem('userProfileId');
            this.$window.sessionStorage.removeItem('screenName');

            // DLS: Added
            this.$location.path('/');
        }

        register() {

            // RegisterBindingModel
            let newUser = {
                email: this.username,
                password: this.password,
                confirmPassword: this.confirmPassword
            };

            this.accountService.registerNewUser(newUser).success((result) => {
                this.$location.path('/login');
            }).error((result) => {
                this.registerMessage = 'Invalid user registration';
            });
        }

        changePassword() {

            // ChangePasswordBindingModel
            let newPassword = {
                oldPassword: this.oldPassword,
                newPassword: this.newPassword,
                confirmPassword: this.confirmPassword
            };

            this.accountService.changePassword(newPassword).success((result) => {
                this.$location.path('/');
            }).error((result) => {
                // http://www.codeproject.com/Articles/825274/ASP-NET-Web-Api-Unwrapping-HTTP-Error-Results-and
                //console.dir(result);
                this.changePasswordMessage = 'Invalid change password';
            });
        }

        isLoggedIn() {
            return this.$window.sessionStorage.getItem('token');
        }

        getUserName() {
            return this.$window.sessionStorage.getItem('userName')
        }

        getUserId() {
            return this.$window.sessionStorage.getItem('userId');
        }

        isAdministrator() {
            return (this.$window.sessionStorage.getItem('roles') == 'Admin');
        }

        getUserProfileId() {
            // Note: Returns null if the User has not yet created his/her Profile.
            return this.$window.sessionStorage.getItem('userProfileId');
        }

        getScreenName() {
            // Note: Returns null if the User has not yet created his/her Profile.
            return this.$window.sessionStorage.getItem('screenName');
        }
        
        constructor(private $http: ng.IHttpService,
                    private $window: ng.IWindowService,
                    private $location: ng.ILocationService,
                    private accountService: RoversListApp.Services.AccountService,
                    private userProfileService: RoversListApp.Services.UserProfileService,
                    private photoService: RoversListApp.Services.PhotoService,
                    private boardService: RoversListApp.Services.BoardService)
        {
            boardService.listBoards().then((result) =>
            {
                this.boards = result.data;
                
                console.log(this.boards);
                this.photoBoards = [];
                this.events = [];
                this.locations = [];
                this.stories = [];
                for (var i = 0; i < this.boards.length; i++)
                {
                    if (this.boards[i]['boardTypeName'] === 'Photo')
                        this.photoBoards.push(this.boards[i]);
                    if (this.boards[i]['boardTypeName'] === 'Event')
                        this.events.push(this.boards[i]);
                    if (this.boards[i]['boardTypeName'] === 'Location')
                        this.locations.push(this.boards[i]);
                    if (this.boards[i]['boardTypeName'] === 'Story')
                        this.stories.push(this.boards[i]);
                }
                this.randomStoryList = randomSort(this.stories);
                console.log(this.randomStoryList);
                this.randomEvents = randomSort(this.events);
            });



            photoService.listPhotos().then((result) =>
            {
                this.photos = result.data;
                this.randomPhotoList = randomSort(this.photos);
                console.dir("photos:" + this.photos[0].photoPath);
                console.dir("random photos:" + randomSort(this.photos)[0].photoPathd);
            }); 

            function randomSort(input)
            {
                let numArray = [];
                let size = input.length - 1;

                for (var i = 0; i <= size; i++)
                {
                    let size = input.length - 1;
                    let randomIndex = Math.floor((Math.random() * (size)));
                    let randValue = input.splice(randomIndex, 1);
                    numArray.push(randValue[0]);
                }
                return numArray;
            }

            function acsendingSort(input, field)
            {
                let numArray = input;
                let size = numArray.length;
                for (let i = 0; i < size; i++)
                {
                    let tmp = numArray[i];
                    for (var j = i - 1; j >= 0 && numArray[j][field] > tmp.field; j--)
                    {
                        numArray[j + 1] = numArray[j];
                    }
                    numArray[j + 1] = tmp;
                    console.log(numArray);
                }
                return numArray;
            }

            function descendingSort(input, field)
            {
                let numArray = input;
                let size = numArray.length - 1;
                for (let i = size; i >= 0; i--)
                {
                    let tmp = numArray[i];
                    for (var j = i + 1; j >= 0 && numArray[j][field] > tmp.field; j++)
                    {
                        numArray[j - 1] = numArray[j];
                    }
                    numArray[j - 1] = tmp;
                }
                return numArray;
            }
           
        }
    }

    angular.module('RoversListApp').controller('AccountController', AccountController);


    class UserProfileController {

        public id
        public userId
        public screenName
        public description
        public photoPath
        public city
        public stateCode
        public email
        public doNotShowEmail
        public contactByEmail
        public cellPhoneNumber
        public doNotShowPhone
        public contactByPhone
        public inactive
        public saveUserProfileMessage

        public states
        
        public statePicklist = {
            availableOptions: [],
            selectedOption: { code: '', name: ''}
        }

        saveUserProfile() {

            // DLS: TBD

            let updatedUserProfile = {
                id: this.id,
                userId: this.userId,
                screenName: this.screenName,
                description: this.description,
                photoPath: this.photoPath,
                city: this.city,
                stateCode: this.statePicklist.selectedOption.code,
                email: this.email,
                doNotShowEmail: this.doNotShowEmail,
                contactByEmail: this.contactByEmail,
                cellPhoneNumber: this.cellPhoneNumber,
                doNotShowPhone: this.doNotShowPhone,
                contactByPhone: this.contactByPhone,
                inactive: this.inactive
            };

            this.userProfileService.saveUserProfile(updatedUserProfile).then((result: any) => {
                // Update Session Storage
                this.$window.sessionStorage.setItem('userProfileId', result.data.id);
                this.$window.sessionStorage.setItem('screenName', result.data.screenName);
            });

        }

        constructor(private $http: ng.IHttpService,
                    private $window: ng.IWindowService,
                    private $location: ng.ILocationService,
                    private userProfileService: RoversListApp.Services.UserProfileService,
                    private stateService: RoversListApp.Services.StateService) {

            // build the stateCode pickList
            this.stateService.listStates().then((result: any) => {
                this.states = result.data;
                let firstState = true;

                for (let state of this.states) {
                    this.statePicklist.availableOptions.push({ code: state.code, name: state.name });
                    if (firstState) {
                        firstState = false;
                        this.statePicklist.selectedOption = { code: state.code, name: state.name };
                    }
                }

                // get the userId of the currently logged in User
                this.userId = this.$window.sessionStorage.getItem('userId');
            
                if (this.$window.sessionStorage.getItem('screenName') == null) {
                    // no UserProfile exists for the currently logged in User
                    this.id = 0;
                    this.screenName = '';
                    this.description = '';
                    this.photoPath = '';
                    this.city = '';
                    this.stateCode = '';
                    this.email = '';
                    this.doNotShowEmail = true;
                    this.contactByEmail = false;
                    this.cellPhoneNumber = '';
                    this.doNotShowPhone = true;
                    this.contactByPhone = false;
                    this.inactive = false;

                } else {
                    // get the existing UserProfile
                    this.userProfileService.getUserProfileByUserId(this.userId)
                        .then((result: any) => {
                            this.id = result.data.id;
                            this.screenName = result.data.screenName;
                            this.description = result.data.description;
                            this.photoPath = result.data.photoPath;
                            this.city = result.data.city;
                            this.stateCode = result.data.stateCode;
                            this.email = result.data.email;
                            this.doNotShowEmail = result.data.doNotShowEmail;
                            this.contactByEmail = result.data.contactByEmail;
                            this.cellPhoneNumber = result.data.cellPhoneNumber;
                            this.doNotShowPhone = result.data.doNotShowPhone;
                            this.contactByPhone = result.data.contactByPhone;
                            this.inactive = result.data.inactive;

                            for (let state of this.states) {
                                if (state.code == this.stateCode) {
                                    this.statePicklist.selectedOption = { code: state.code, name: state.name };
                                }
                            }
                        
                        });
                }
            });
        };

    }

    angular.module('RoversListApp').controller('UserProfileController', UserProfileController);
}