var RoversListApp;
(function (RoversListApp) {
    var Controllers;
    (function (Controllers) {
        var AccountController = (function () {
            function AccountController($http, $window, $location, accountService) {
                this.$http = $http;
                this.$window = $window;
                this.$location = $location;
                this.accountService = accountService;
            }
            AccountController.prototype.login = function () {
                var _this = this;
                var data = "grant_type=password&username=" + this.username + "&password=" + this.password;
                // RGD: add full path to compilation WevAPI localhost from /Token
                this.$http.post('/Token', data, {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (result) {
                    _this.$window.sessionStorage.setItem('token', result.access_token);
                    // DLS: Added custom authentication properties.
                    _this.$window.sessionStorage.setItem('userName', result.userName);
                    _this.$window.sessionStorage.setItem('userId', result.userId);
                    _this.$window.sessionStorage.setItem('roles', result.roles);
                    _this.$location.path('/');
                }).error(function () {
                    _this.loginMessage = 'Invalid user name/password';
                });
            };
            AccountController.prototype.logout = function () {
                this.$window.sessionStorage.removeItem('token');
                // DLS: Added the removal of the custom authentication properties
                this.$window.sessionStorage.removeItem('userName');
                this.$window.sessionStorage.removeItem('userId');
                this.$window.sessionStorage.removeItem('roles');
                // DLS: Added
                this.$location.path('/');
            };
            AccountController.prototype.register = function () {
                var _this = this;
                // RegisterBindingModel
                var newUser = {
                    email: this.username,
                    password: this.password,
                    confirmPassword: this.confirmPassword
                };
                this.accountService.registerNewUser(newUser).success(function (result) {
                    _this.$location.path('/login');
                }).error(function (result) {
                    _this.registerMessage = 'Invalid user registration';
                });
            };
            AccountController.prototype.changePassword = function () {
                var _this = this;
                // ChangePasswordBindingModel
                var newPassword = {
                    oldPassword: this.oldPassword,
                    newPassword: this.newPassword,
                    confirmPassword: this.confirmPassword
                };
                this.accountService.changePassword(newPassword).success(function (result) {
                    _this.$location.path('/');
                }).error(function (result) {
                    // http://www.codeproject.com/Articles/825274/ASP-NET-Web-Api-Unwrapping-HTTP-Error-Results-and
                    //console.dir(result);
                    _this.changePasswordMessage = 'Invalid change password';
                });
            };
            AccountController.prototype.isLoggedIn = function () {
                return this.$window.sessionStorage.getItem('token');
            };
            AccountController.prototype.getUserName = function () {
                return this.$window.sessionStorage.getItem('userName');
            };
            AccountController.prototype.getUserId = function () {
                return this.$window.sessionStorage.getItem('userId');
            };
            AccountController.prototype.isAdministrator = function () {
                return (this.$window.sessionStorage.getItem('roles') == 'Admin');
            };
            return AccountController;
        })();
        angular.module('RoversListApp').controller('AccountController', AccountController);
        var UserProfileController = (function () {
            function UserProfileController() {
            }
            return UserProfileController;
        })();
        angular.module('RoversListApp').controller('UserProfileController', UserProfileController);
    })(Controllers = RoversListApp.Controllers || (RoversListApp.Controllers = {}));
})(RoversListApp || (RoversListApp = {}));
//# sourceMappingURL=controllers.js.map