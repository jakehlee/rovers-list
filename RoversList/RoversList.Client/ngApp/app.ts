﻿namespace RoversListApp {

    angular.module('RoversListApp', ['ngResource', 'ngRoute', 'ui.bootstrap'])
        .config(($routeProvider: ng.route.IRouteProvider, $locationProvider: ng.ILocationProvider) => {

            $routeProvider
                .when('/', {
                    templateUrl: '/ngApp/Views/main.html'
                }).when('/members', {
                    templateUrl: '/ngApp/Views/members.html'
                }).when('/boards', {
                    templateUrl: '/ngApp/Views/list.html',
                    controller: 'BoardListController as vm'
                }).when('/account', {
                    templateUrl: '/ngApp/Views/account.html',
                    controller: 'AccountController as account'
                }).when('/login', {
                    templateUrl: '/ngApp/Views/login.html',
                    controller: 'AccountController as account'
                }).when('/register', {
                    templateUrl: '/ngApp/Views/register.html',
                    controller: 'AccountController as account'
                }).when('/userProfile', {
                    templateUrl: '/ngApp/Views/userProfile.html',
                    controller: 'UserProfileController as vm'
                }).when('/board/:boardId/photo/:id', {
                    templateUrl: '/ngApp/Views/photo.html',
                    controller: 'PhotoController as vm'
                }).when('/board/:id', {
                    templateUrl: '/ngApp/Views/board.html',
                    controller: 'BoardController as vm'
                }).when('/:id', {
                    templateUrl: '/ngApp/Views/details.html',
                    controller: 'BoardDetailsController as vm'
                })
                .otherwise('/'); 

            $locationProvider.html5Mode(true);
        })
        // RGD: Add full path to WebAPI compilation
        .constant('WebApiURL', '');

    angular.module('RoversListApp').factory('authInterceptor', (
        $q: ng.IQService,
        $window: ng.IWindowService,
        $location: ng.ILocationService
        ) =>
        ({
            request: function (config) {
                config.headers = config.headers || {};
                let token = $window.sessionStorage.getItem('token');
                if (token) {
                    config.headers.Authorization = 'Bearer ' + token;
                }
                return config;
            },
            response: function (response) {
                if (response.status === 401) {
                    $location.path('/login');
                }
                return response || $q.when(response);
            }
        })
        );

    angular.module('RoversListApp').config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
    });

}  