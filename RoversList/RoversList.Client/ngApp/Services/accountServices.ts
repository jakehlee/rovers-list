﻿namespace RoversListApp.Services {

    export class AccountService {

        public registerNewUser(newUser) {
            return this.$http.post(this.WebApiURL + '/api/account/register', newUser);
        }

        public changePassword(newPassword) {
            return this.$http.post(this.WebApiURL + '/api/account/changePassword', newPassword);
        }

        constructor(private $http: ng.IHttpService, private WebApiURL) { }
    }

    angular.module('RoversListApp').service('accountService', AccountService);


    export class UserProfileService {

        public listUserProfiles() {
            return this.$http.get(this.WebApiURL + '/api/userProfiles');
        }

        public getUserProfile(id) {
            return this.$http.get(this.WebApiURL + '/api/userProfiles/' + id);
        }

        public getUserProfileByUserId(userId) {
            return this.$http.get(this.WebApiURL + '/api/userProfiles/' + userId);
        }

        public getUserProfileByScreenName(screenName) {
            return this.$http.get(this.WebApiURL + '/api/userProfiles/' + screenName);
        }

        public saveUserProfile(userProfile) {
            return this.$http.post(this.WebApiURL + '/api/userProfiles', userProfile);
        }

        constructor(private $http: ng.IHttpService, private WebApiURL) { }
    }

    angular.module('RoversListApp').service('userProfileService', UserProfileService);


    export class StateService {

        public listStates() {
            return this.$http.get(this.WebApiURL + '/api/states');
        }

        constructor(private $http: ng.IHttpService, private WebApiURL) { }
    }

    angular.module('RoversListApp').service('stateService', StateService);


}