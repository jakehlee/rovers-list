﻿namespace RoversListApp.Services {

    export class BoardService {

        public listBoards() {
            return this.$http.get(this.WebApiURL + '/api/boards');
        }

        public getBoard(id) {
            return this.$http.get(this.WebApiURL + '/api/boards/' + id);
        }

        public saveBoard(board) {
            return this.$http.post(this.WebApiURL + '/api/boards', board);
        }

        constructor(private $http: ng.IHttpService, private WebApiURL) { }
    }

    angular.module('RoversListApp').service('boardService', BoardService);


    export class CommentService {

        public listComments() {
            return this.$http.get(this.WebApiURL + '/api/comments');
        }

        public getComment(id) {
            return this.$http.get(this.WebApiURL + '/api/comments/' + id);
        }

        public saveComment(comment) {
            return this.$http.post(this.WebApiURL + '/api/comments', comment);
        }

        constructor(private $http: ng.IHttpService, private WebApiURL) { }
    }

    angular.module('RoversListApp').service('commentService', CommentService);


    export class PhotoService {

        public listPhotos() {
            return this.$http.get(this.WebApiURL + '/api/photos');
        }

        public getPhoto(id) {
            return this.$http.get(this.WebApiURL + '/api/photos/' + id);
        }

        public savePhoto(photo) {
            return this.$http.post(this.WebApiURL + '/api/photos', photo);
        }

        constructor(private $http: ng.IHttpService, private WebApiURL) { }
    }

    angular.module('RoversListApp').service('photoService', PhotoService);


    export class ReviewService {

        public listReviews() {
            return this.$http.get(this.WebApiURL + '/api/reviews');
        }

        public getReview(id) {
            return this.$http.get(this.WebApiURL + '/api/reviews/' + id);
        }

        public saveReview(review) {
            return this.$http.post(this.WebApiURL + '/api/reviews', review);
        }

        constructor(private $http: ng.IHttpService, private WebApiURL) { }
    }

    angular.module('RoversListApp').service('reviewService', ReviewService);


}