﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoversList.Client.DomainClasses
{
    public class Video
    {
        public int Id { get; set; }

        [Required]
        [MinLength(10)]
        [MaxLength(70)]
        [Index("VideoNameIndex", IsUnique = true)]
        public string Name { get; set; }

        public string Description { get; set; }
        public string URL { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool Inactive { get; set; }

        public int ClickCount { get; set; }

    }
}