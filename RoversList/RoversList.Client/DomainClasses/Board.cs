﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoversList.Client.DomainClasses
{
    public class Board
    {
        public int Id { get; set; }

        public int BoardTypeId { get; set; }
        [ForeignKey("BoardTypeId")]
        public BoardType BoardType { get; set; }

        // Last updater of the board
        public int UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public UserProfile UserProfile { get; set; }

        public bool OnlyCreatorCanUpdateMainBoardInfo { get; set; }
        public bool OnlyCreatorCanAddBoardPhotos { get; set; }

        public int LocationId { get; set; }
        [ForeignKey("LocationId")]
        public Location Location { get; set; }

        [Required]
        [MinLength(10)]
        [MaxLength(70)]
        [Index("BoardNameIndex", IsUnique = true)]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public string URL { get; set; }  // External website URL
        public string PhotoPath { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public DateTime EventDate { get; set; }
        public string EventHours { get; set; }  // free-form text
        public string EventSponsors { get; set; }  // free-form text
        public string VolunteerOpportunities { get; set; }  // free-form text

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool Inactive { get; set; }

        public int BoardFlagCount { get; set; }
        public int CommentCount { get; set; }
        public int PhotoCount { get; set; }
        public int ReviewCount { get; set; }
        public decimal ReviewAverage { get; set; }

        public ICollection<BoardFlag> BoardFlags { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Photo> Photos { get; set; }
        public ICollection<Review> Reviews { get; set; }

    }
}