﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoversList.Client.DomainClasses
{
    public class ActivityLog
    {
        public int Id { get; set; }

        public int UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public UserProfile UserProfile { get; set; }

        public string UserProfileURL { get; set; }
        public string ActivityText { get; set; }
        public string ActivityURLText { get; set; }
        public string ActivityURL { get; set; }

        public DateTime CreatedOn { get; set; }

    }
}