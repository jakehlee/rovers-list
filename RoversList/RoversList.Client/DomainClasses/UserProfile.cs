﻿using RoversList.Client.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoversList.Client.DomainClasses
{
    public class UserProfile
    {
        public int Id { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }

        [Required]
        [MinLength(4)]
        [MaxLength(20)]
        [Index("ScreenNameIndex", IsUnique = true)]
        public string ScreenName { get; set; }

        public string Description { get; set; }
        public string PhotoPath { get; set; }

        [Required]        
        public string City { get; set; }

        [Required]      
        public string StateCode { get; set; }

        public string Email { get; set; } // Contact Email (versus Login Email/UserName)
        public bool DoNotShowEmail { get; set; }
        public bool ContactByEmail { get; set; }

        public string CellPhoneNumber { get; set; }
        public bool DoNotShowPhone { get; set; }
        public bool ContactByPhone { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool Inactive { get; set; }

        public ICollection<UserTag> UserTags { get; set; }

        public ICollection<Board> Boards { get; set; }        
        public ICollection<BoardFlag> BoardFlags { get; set; }

        public ICollection<Comment> Comments { get; set; }
        public ICollection<CommentFlag> CommentFlags { get; set; }

        public ICollection<Photo> Photos { get; set; }
        public ICollection<PhotoFlag> PhotoFlags { get; set; }

        public ICollection<Review> Reviews { get; set; }
        public ICollection<ReviewFlag> ReviewFlags { get; set; }

    }
}