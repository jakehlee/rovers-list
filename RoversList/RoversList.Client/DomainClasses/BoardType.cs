﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.DomainClasses
{
    public class BoardType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Board> Boards { get; set; }

    }
}