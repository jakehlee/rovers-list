﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoversList.Client.DomainClasses
{
    public class Ad
    {
        public int Id { get; set; }

        public int VendorId { get; set; }
        [ForeignKey("VendorId")]
        public Vendor Vendor { get; set; }

        [Required]
        [MinLength(10)]
        [MaxLength(70)]
        [Index("AdNameIndex", IsUnique = true)]
        public string Name { get; set; }

        public string PhotoPath { get; set; }
        public string URL { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public int ClickCount { get; set; }

    }
}