﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoversList.Client.DomainClasses
{
    public class Location
    {
        public int Id { get; set; }

        [Required]
        [MinLength(4)]  // To allow Name = "None"
        [MaxLength(50)]
        [Index("LocationNameIndex", IsUnique = true)]
        public string Name { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string PostalCode { get; set; }

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool Inactive { get; set; }

        public ICollection<Board> Boards { get; set; }

    }
}