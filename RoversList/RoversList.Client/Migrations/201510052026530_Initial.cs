namespace RoversList.Client.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserProfileId = c.Int(nullable: false),
                        UserProfileURL = c.String(),
                        ActivityText = c.String(),
                        ActivityURLText = c.String(),
                        ActivityURL = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        ScreenName = c.String(nullable: false, maxLength: 20),
                        Description = c.String(),
                        PhotoPath = c.String(),
                        City = c.String(nullable: false),
                        StateCode = c.String(nullable: false),
                        Email = c.String(),
                        DoNotShowEmail = c.Boolean(nullable: false),
                        ContactByEmail = c.Boolean(nullable: false),
                        CellPhoneNumber = c.String(),
                        DoNotShowPhone = c.Boolean(nullable: false),
                        ContactByPhone = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ScreenName, unique: true, name: "ScreenNameIndex");
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.BoardFlags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BoardId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        FlagText = c.String(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Boards", t => t.BoardId)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.BoardId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.Boards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BoardTypeId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        OnlyCreatorCanUpdateMainBoardInfo = c.Boolean(nullable: false),
                        OnlyCreatorCanAddBoardPhotos = c.Boolean(nullable: false),
                        LocationId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 70),
                        Description = c.String(nullable: false),
                        URL = c.String(),
                        PhotoPath = c.String(),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        EventDate = c.DateTime(nullable: false),
                        EventHours = c.String(),
                        EventSponsors = c.String(),
                        VolunteerOpportunities = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                        BoardFlagCount = c.Int(nullable: false),
                        CommentCount = c.Int(nullable: false),
                        PhotoCount = c.Int(nullable: false),
                        ReviewCount = c.Int(nullable: false),
                        ReviewAverage = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BoardTypes", t => t.BoardTypeId)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.BoardTypeId)
                .Index(t => t.UserProfileId)
                .Index(t => t.LocationId)
                .Index(t => t.Name, unique: true, name: "BoardNameIndex");
            
            CreateTable(
                "dbo.BoardTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BoardId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        CommentText = c.String(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                        CommentFlagCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Boards", t => t.BoardId)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.BoardId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.CommentFlags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CommentId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        FlagText = c.String(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Comments", t => t.CommentId)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.CommentId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        City = c.String(),
                        StateCode = c.String(),
                        PostalCode = c.String(),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 9),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 9),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "LocationNameIndex");
            
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BoardId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        PhotoPath = c.String(nullable: false),
                        Description = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                        PhotoFlagCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Boards", t => t.BoardId)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.BoardId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.PhotoFlags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhotoId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        FlagText = c.String(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Photos", t => t.PhotoId)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.PhotoId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BoardId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        ReviewText = c.String(nullable: false),
                        Rating = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                        ReviewFlagCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Boards", t => t.BoardId)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.BoardId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.ReviewFlags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReviewId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        FlagText = c.String(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Reviews", t => t.ReviewId)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.ReviewId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.UserTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserProfileId = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        URL = c.String(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.Ads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VendorId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 70),
                        PhotoPath = c.String(),
                        URL = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        ClickCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vendors", t => t.VendorId)
                .Index(t => t.VendorId)
                .Index(t => t.Name, unique: true, name: "AdNameIndex");
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        ContactName = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        City = c.String(),
                        StateCode = c.String(),
                        PostalCode = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "VendorNameIndex");
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 70),
                        Description = c.String(),
                        URL = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                        ClickCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "VideoNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Ads", "VendorId", "dbo.Vendors");
            DropForeignKey("dbo.ActivityLogs", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.UserTags", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.BoardFlags", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.Boards", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.Reviews", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.ReviewFlags", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.ReviewFlags", "ReviewId", "dbo.Reviews");
            DropForeignKey("dbo.Reviews", "BoardId", "dbo.Boards");
            DropForeignKey("dbo.Photos", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.PhotoFlags", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.PhotoFlags", "PhotoId", "dbo.Photos");
            DropForeignKey("dbo.Photos", "BoardId", "dbo.Boards");
            DropForeignKey("dbo.Boards", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Comments", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.CommentFlags", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.CommentFlags", "CommentId", "dbo.Comments");
            DropForeignKey("dbo.Comments", "BoardId", "dbo.Boards");
            DropForeignKey("dbo.Boards", "BoardTypeId", "dbo.BoardTypes");
            DropForeignKey("dbo.BoardFlags", "BoardId", "dbo.Boards");
            DropForeignKey("dbo.UserProfiles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Videos", "VideoNameIndex");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Vendors", "VendorNameIndex");
            DropIndex("dbo.Ads", "AdNameIndex");
            DropIndex("dbo.Ads", new[] { "VendorId" });
            DropIndex("dbo.UserTags", new[] { "UserProfileId" });
            DropIndex("dbo.ReviewFlags", new[] { "UserProfileId" });
            DropIndex("dbo.ReviewFlags", new[] { "ReviewId" });
            DropIndex("dbo.Reviews", new[] { "UserProfileId" });
            DropIndex("dbo.Reviews", new[] { "BoardId" });
            DropIndex("dbo.PhotoFlags", new[] { "UserProfileId" });
            DropIndex("dbo.PhotoFlags", new[] { "PhotoId" });
            DropIndex("dbo.Photos", new[] { "UserProfileId" });
            DropIndex("dbo.Photos", new[] { "BoardId" });
            DropIndex("dbo.Locations", "LocationNameIndex");
            DropIndex("dbo.CommentFlags", new[] { "UserProfileId" });
            DropIndex("dbo.CommentFlags", new[] { "CommentId" });
            DropIndex("dbo.Comments", new[] { "UserProfileId" });
            DropIndex("dbo.Comments", new[] { "BoardId" });
            DropIndex("dbo.Boards", "BoardNameIndex");
            DropIndex("dbo.Boards", new[] { "LocationId" });
            DropIndex("dbo.Boards", new[] { "UserProfileId" });
            DropIndex("dbo.Boards", new[] { "BoardTypeId" });
            DropIndex("dbo.BoardFlags", new[] { "UserProfileId" });
            DropIndex("dbo.BoardFlags", new[] { "BoardId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.UserProfiles", "ScreenNameIndex");
            DropIndex("dbo.UserProfiles", new[] { "UserId" });
            DropIndex("dbo.ActivityLogs", new[] { "UserProfileId" });
            DropTable("dbo.Videos");
            DropTable("dbo.States");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Vendors");
            DropTable("dbo.Ads");
            DropTable("dbo.UserTags");
            DropTable("dbo.ReviewFlags");
            DropTable("dbo.Reviews");
            DropTable("dbo.PhotoFlags");
            DropTable("dbo.Photos");
            DropTable("dbo.Locations");
            DropTable("dbo.CommentFlags");
            DropTable("dbo.Comments");
            DropTable("dbo.BoardTypes");
            DropTable("dbo.Boards");
            DropTable("dbo.BoardFlags");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.UserProfiles");
            DropTable("dbo.ActivityLogs");
        }
    }
}
