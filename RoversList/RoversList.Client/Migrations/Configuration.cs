namespace RoversList.Client.Migrations
{
    using RoversList.Client.DomainClasses;
    using RoversList.Client.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Configuration;
    using System.Security.Claims;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<RoversList.Client.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RoversList.Client.Models.ApplicationDbContext context)
        {
            context.Database.ExecuteSqlCommand("delete from ActivityLogs");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('ActivityLogs', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from CommentFlags");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('CommentFlags', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from Comments");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Comments', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from PhotoFlags");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('PhotoFlags', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from Photos");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Photos', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from ReviewFlags");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('ReviewFlags', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from Reviews");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Reviews', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from BoardFlags");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('BoardFlags', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from Boards");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Boards', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from UserTags");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('UserTags', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from UserProfiles");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('UserProfiles', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from BoardTypes");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('BoardTypes', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from Locations");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Locations', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from Ads");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Ads', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from Vendors");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Vendors', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from Videos");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Videos', RESEED, 0)");

            context.Database.ExecuteSqlCommand("delete from States");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('States', RESEED, 0)");

            // need to Save Changes for the identity reseedings.
            context.SaveChanges();

            var states = new State[] {
                new State { Code = "AK", Name="AK Alaska" },
                new State { Code = "AL", Name="AL Alabama" },
                new State { Code = "AR", Name="AR Arkansas" },
                new State { Code = "AZ", Name="AZ Arizona" },
                new State { Code = "CA", Name="CA California" },
                new State { Code = "CO", Name="CO Colorado" },
                new State { Code = "CT", Name="CT Connecticut" },
                new State { Code = "DC", Name="DC District of Columbia" },
                new State { Code = "DE", Name="DE Delaware" },
                new State { Code = "FL", Name="FL Florida" },
                new State { Code = "GA", Name="GA Georgia" },
                new State { Code = "HI", Name="HI Hawaii" },
                new State { Code = "IA", Name="IA Iowa" },
                new State { Code = "ID", Name="ID Idaho" },
                new State { Code = "IL", Name="IL Illinois" },
                new State { Code = "IN", Name="IN Indiana" },
                new State { Code = "KS", Name="KS Kansas" },
                new State { Code = "KY", Name="KY Kentucky" },
                new State { Code = "LA", Name="LA Louisiana" },
                new State { Code = "MA", Name="MA Massachusetts" },
                new State { Code = "MD", Name="MD Maryland" },
                new State { Code = "ME", Name="ME Maine" },
                new State { Code = "MI", Name="MI Michigan" },
                new State { Code = "MN", Name="MN Minnesota" },
                new State { Code = "MO", Name="MO Missouri" },
                new State { Code = "MS", Name="MS Mississippi" },
                new State { Code = "MT", Name="MT Montana" },
                new State { Code = "NC", Name="NC North Carolina" },
                new State { Code = "ND", Name="ND North Dakota" },
                new State { Code = "NE", Name="NE Nebraska" },
                new State { Code = "NH", Name="NH New Hampshire" },
                new State { Code = "NJ", Name="NJ New Jersey" },
                new State { Code = "NM", Name="NM New Mexico" },
                new State { Code = "NV", Name="NV Nevada" },
                new State { Code = "NY", Name="NY New York" },
                new State { Code = "OH", Name="OH Ohio" },
                new State { Code = "OK", Name="OK Oklahoma" },
                new State { Code = "OR", Name="OR Oregon" },
                new State { Code = "PA", Name="PA Pennsylvania" },
                new State { Code = "RI", Name="RI Rhode Island" },
                new State { Code = "SC", Name="SC South Carolina" },
                new State { Code = "SD", Name="SD South Dakota" },
                new State { Code = "TN", Name="TN Tennessee" },
                new State { Code = "TX", Name="TX Texas" },
                new State { Code = "UT", Name="UT Utah" },
                new State { Code = "VA", Name="VA Virginia" },
                new State { Code = "VT", Name="VT Vermont" },
                new State { Code = "WA", Name="WA Washington" },
                new State { Code = "WI", Name="WI Wisconsin" },
                new State { Code = "WV", Name="WV West Virginia" },
                new State { Code = "WY", Name="WY Wyoming" }
            };

            context.States.AddOrUpdate(s => s.Code, states);

            var boardTypes = new BoardType[] {
                new BoardType {Name = "Event"},
                new BoardType {Name = "Location"},
                new BoardType {Name = "Photo"},
                new BoardType {Name = "Story"}
            };

            context.BoardTypes.AddOrUpdate(bt => bt.Name, boardTypes);

            var locations = new Location[] {
                new Location {
                    Name = "None",
                    AddressLine1 = "",
                    AddressLine2 = "",
                    City = "",
                    StateCode = "",
                    PostalCode = "",
                    Latitude = 0m,
                    Longitude = 0m,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false      
                },
                new Location {
                    Name = "Petco on Northtowne, Reno, NV",
                    AddressLine1 = "2970 Northtowne Ln",
                    AddressLine2 = "",
                    City = "Reno",
                    StateCode = "NV",
                    PostalCode = "89512",
                    Latitude = 39.5581880m,
                    Longitude = -119.7811560m,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false      
                },
                new Location {
                    Name = "Petco on South Virginia, Reno, NV",
                    AddressLine1 = "5565 S Virginia St",
                    AddressLine2 = "",
                    City = "Reno",
                    StateCode = "NV",
                    PostalCode = "89502",
                    Latitude = 39.4751480m,
                    Longitude = -119.7898490m,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false      
                },
                new Location {
                    Name = "Hidden Valley Regional Park, Reno, NV",
                    AddressLine1 = "Parkway Dr",
                    AddressLine2 = "",
                    City = "Reno",
                    StateCode = "NV",
                    PostalCode = "89502",
                    Latitude = 39.4934710m,
                    Longitude = -119.7144830m,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false      
                },
                new Location {
                    Name = "San Rafael Regional Park, Reno, NV",
                    AddressLine1 = "1595 North Sierra St",
                    AddressLine2 = "",
                    City = "Reno",
                    StateCode = "NV",
                    PostalCode = "89503",
                    Latitude = 39.5457300m,
                    Longitude = -119.8239740m,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false      
                },
                new Location {
                    Name = "Nevada Humane Society, Reno, NV",
                    AddressLine1 = "2825 Longley Ln",
                    AddressLine2 = "",
                    City = "Reno",
                    StateCode = "NV",
                    PostalCode = "89502",
                    Latitude = 39.493657m,
                    Longitude = -119.7565150m,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false      
                },
                new Location {
                    Name = "Chihuahua Rescue Truckee Meadows, Reno, NV",
                    AddressLine1 = "5303 Louie Ln",
                    AddressLine2 = "",
                    City = "Reno",
                    StateCode = "NV",
                    PostalCode = "89511",
                    Latitude = 39.474917m,
                    Longitude = -119.7682959m,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false      
                } //,
                //new Location {
                //    Name = "",
                //    AddressLine1 = "",
                //    AddressLine2 = "",
                //    City = "",
                //    StateCode = "",
                //    PostalCode = "",
                //    Latitude = 0,
                //    Longitude = 0,
                //    CreatedOn = DateTime.Now,
                //    UpdatedOn = DateTime.Now,
                //    Inactive = false      
                //},
            };

            context.Locations.AddOrUpdate(l => l.Name, locations);
            context.SaveChanges();

            // The following seeds AspNetUser and AspNetUserClaims data:

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new ApplicationUserManager(userStore);

            var seedPassword = "Secret123!";

            //------------------------------------------------------------
            var user = userManager.FindByName("DianeLSmith@cox.net");
            if (user == null)
            {
                // create user
                user = new ApplicationUser
                {
                    UserName = "DianeLSmith@cox.net",
                    Email = "DianeLSmith@cox.net"
                };
                userManager.Create(user, seedPassword);

                // add Admin role/claim
                userManager.AddClaim(user.Id, new Claim(ClaimTypes.Role, "Admin"));
            }

            user = userManager.FindByName("smithdl@cox.net");
            if (user == null)
            {
                // create user
                user = new ApplicationUser
                {
                    UserName = "smithdl@cox.net",
                    Email = "smithdl@cox.net"
                };
                userManager.Create(user, seedPassword);
            }

            //------------------------------------------------------------
            user = userManager.FindByName("RonDanish@charter.net");
            if (user == null)
            {
                // create user
                user = new ApplicationUser
                {
                    UserName = "RonDanish@charter.net",
                    Email = "RonDanish@charter.net"
                };
                userManager.Create(user, seedPassword);

                // add Admin role/claim
                userManager.AddClaim(user.Id, new Claim(ClaimTypes.Role, "Admin"));
            }

            //------------------------------------------------------------
            user = userManager.FindByName("Jake481Lee@gmail.com");
            if (user == null)
            {
                // create user
                user = new ApplicationUser
                {
                    UserName = "Jake481Lee@gmail.com",
                    Email = "Jake481Lee@gmail.com"
                };
                userManager.Create(user, seedPassword);

                // add Admin role/claim
                userManager.AddClaim(user.Id, new Claim(ClaimTypes.Role, "Admin"));
            }

            user = userManager.FindByName("Jake4Jinny@naver.com");
            if (user == null)
            {
                // create user
                user = new ApplicationUser
                {
                    UserName = "Jake4Jinny@naver.com",
                    Email = "Jake4Jinny@naver.com"
                };
                userManager.Create(user, seedPassword);
            }

            var userProfiles = new UserProfile[] {
                new UserProfile {
                    UserId = userManager.FindByName("DianeLSmith@cox.net").Id,
                    ScreenName = "LadyDi",
                    Description = "Lover of all dogs, especially smaller breeds and labs.",
                    PhotoPath = "",
                    City = "Virginia Beach",
                    StateCode = "VA",
                    Email = "diane.l.smith@cox.net",
                    DoNotShowEmail = true,
                    ContactByEmail = true,
                    CellPhoneNumber = "",
                    DoNotShowPhone = true,
                    ContactByPhone = false,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false     
                },
                new UserProfile {
                    UserId = userManager.FindByName("smithdl@cox.net").Id,
                    ScreenName = "DLSmith",
                    Description = "Favorite breeds are Terriers and Cocker Spaniels.",
                    PhotoPath = "",
                    City = "Virginia Beach",
                    StateCode = "VA",
                    Email = "quietdi@cox.net",
                    DoNotShowEmail = true,
                    ContactByEmail = true,
                    CellPhoneNumber = "",
                    DoNotShowPhone = true,
                    ContactByPhone = false,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false     
                },
                new UserProfile {
                    UserId = userManager.FindByName("RonDanish@charter.net").Id,
                    ScreenName = "RonMan",
                    Description = "Ron the Man",
                    PhotoPath = "",
                    City = "Reno",
                    StateCode = "NV",
                    Email = "RonDanish@charter.net",
                    DoNotShowEmail = false,
                    ContactByEmail = true,
                    CellPhoneNumber = "775-846-2519",
                    DoNotShowPhone = true,
                    ContactByPhone = true,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false     
                },
                new UserProfile {
                    UserId = userManager.FindByName("Jake481Lee@gmail.com").Id,
                    ScreenName = "Jake",
                    Description = "Golden Retriever fan!",
                    PhotoPath = "",
                    City = "Redmond",
                    StateCode = "WA",
                    Email = "",
                    DoNotShowEmail = true,
                    ContactByEmail = false,
                    CellPhoneNumber = "",
                    DoNotShowPhone = true,
                    ContactByPhone = false,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false     
                },
                new UserProfile {
                    UserId = userManager.FindByName("Jake4Jinny@naver.com").Id,
                    ScreenName = "Jake4Jinny",
                    Description = "We love dogs.",
                    PhotoPath = "",
                    City = "Redmond",
                    StateCode = "WA",
                    Email = "",
                    DoNotShowEmail = true,
                    ContactByEmail = false,
                    CellPhoneNumber = "",
                    DoNotShowPhone = true,
                    ContactByPhone = false,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false     
                }
            };

            context.UserProfiles.AddOrUpdate(up => up.ScreenName, userProfiles);
            context.SaveChanges();

            var boards = new Board[] {
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Location").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "Petco on Northtowne, Reno, NV").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "LadyDi").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Petco on Northtowne, Reno, NV",
                    Description = "Petco pet stores in Reno, NV offer a wide selection of top quality products to meet the needs of a variety of pets. High quality foods are available for nearly all pet types whether you have a dog, cat, reptile, fish, small animal or feathered friend. For your dog, find everything from dog tags and collars to beds and gates, bark control and everything in between. You can also find a variety of premium pet services available at Petco. These include full service dog grooming salons, positive dog training classes, pet vaccinations and more. If you're looking to give a pet a forever home, you can find out when the next pet adoption event will take place at your local store. ",
                    URL = "http://stores.petco.com/nv/reno/pet-supplies-reno-nv-310.html",
                    PhotoPath = "http://rstatic.petco.com/locations/1_11869_Petco-Family-of-Brands-2.jpg",
                    Email = "",
                    PhoneNumber = "775-673-9200",
                    EventDate = DateTime.Now,
                    EventHours = "",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Location").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "Petco on South Virginia, Reno, NV").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "LadyDi").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Petco on South Virginia, Reno, NV",
                    Description = "Petco pet stores in Reno, NV offer a wide selection of top quality products to meet the needs of a variety of pets. High quality foods are available for nearly all pet types whether you have a dog, cat, reptile, fish, small animal or feathered friend. For your dog, find everything from dog tags and collars to beds and gates, bark control and everything in between. You can also find a variety of premium pet services available at Petco. These include full service dog grooming salons, positive dog training classes, pet vaccinations and more. If you're looking to give a pet a forever home, you can find out when the next pet adoption event will take place at your local store.",
                    URL = "http://stores.petco.com/nv/reno/pet-supplies-reno-nv-304.html",
                    PhotoPath = "http://rstatic.petco.com/locations/1_11869_Petco-Family-of-Brands-2.jpg",
                    Email = "",
                    PhoneNumber = "775-829-9200",
                    EventDate = DateTime.Now,
                    EventHours = "",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Location").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "Hidden Valley Regional Park, Reno, NV").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Hidden Valley Regional Park, Reno, NV",
                    Description = "Hidden Valley is a scenic multi-purpose park that dog lovers love to visit.",
                    URL = "http://www.yelp.com/biz/hidden-valley-regional-park-reno",
                    PhotoPath = "http://s3-media3.fl.yelpcdn.com/bphoto/gVZoYB7qXdLAIcZwBMk01Q/ls.jpg",
                    Email = "",
                    PhoneNumber = "",
                    EventDate = DateTime.Now,
                    EventHours = "",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Location").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "San Rafael Regional Park, Reno, NV").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "San Rafael Regional Park, Reno, NV",
                    Description = "Nestled in the center of Reno, this park is just minutes away from downtown Reno and the University of Nevada (Reno).",
                    URL = "https://www.washoecounty.us/parks/parks/park_directory/peavine_district/rancho_san_rafael_regional_park.php",
                    PhotoPath = "https://www.washoecounty.us/_files/img/washoe-channel-tv.jpg",
                    Email = "",
                    PhoneNumber = "",
                    EventDate = DateTime.Now,
                    EventHours = "",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Story").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "None").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Serena, handicapped dog gets her wheelchair!",
                    Description = "Serena, handicapped dog gets her wheelchair!",
                    URL = "",
                    PhotoPath = "",
                    Email = "",
                    PhoneNumber = "",
                    EventDate = DateTime.Now,
                    EventHours = "",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Story").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "None").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Scooter on wheels",
                    Description = "Last year I took little Scooter, an obese doxi on wheels, to the Pet Blessing. She belonged to an elderly couple who couldn't take care of her anymore. She was a Very special needs dog and I knew that pretty soon I would have to take her myself. Not so good. I met Mary at the blessing, and we walked around and introduced her and I gave away my cards, and finally we had the blessing. It was very magical, Mary and I both cried as we surrounded little Scooter. Afterward we walked around a little more, Mary bought a book from Best Friends, and we headed to the parking lot. We had been there about an hour and a half. Just as we were approaching the walk, a lady coming from the lot met us, stopped cold, and said, oh my! We told her Scooter was looking for a home. She said this must be meant to be, and she took Scooter for a walk. The short story, Scooter was adopted within 2 weeks by this lady. It was the Miracle of the Pet Blessing. This year I may go down there with little one-eyed Jake, who isn't necessarily looking for a home, but you never know. No, I don't do the walks, it's too much for me. Here's Scooter.",
                    URL = "https://www.petfinder.com/petdetail/14606957",
                    PhotoPath = "",
                    Email = "",
                    PhoneNumber = "",
                    EventDate = DateTime.Now,
                    EventHours = "",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Location").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "Nevada Humane Society, Reno, NV").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "LadyDi").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Nevada Humane Society, Reno, NV",
                    Description = "Northern Nevada residents and their pets are fortunate to have the services of Nevada Humane Society (NHS), a highly professional and nationally recognized no-kill animal shelter. From its start in 1932 to help alleviate the suffering of stray animals, Nevada Humane Society has grown to include programs and services designed to address the needs of all animals and to provide support, education, and assistance for people that care about them.",
                    URL = "http://nevadahumanesociety.org/",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_2_cbd6c73c-008d-46c9-8fe1-43e40125818e_NevadaHumaneSociety_logo.jpg",
                    Email = "",
                    PhoneNumber = "775-856-2000",
                    EventDate = DateTime.Now,
                    EventHours = "",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Location").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "Chihuahua Rescue Truckee Meadows, Reno, NV").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "LadyDi").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Chihuahua Rescue Truckee Meadows, Reno, NV",
                    Description = "Chihuahua Rescue Truckee Meadows, Inc., is a non-profit 501(c)(3) organization in the northern Nevada area. We often take special needs dogs, help those senior dogs that would die in the shelter and find a foster home and work with some dogs with behavior issues, rehabilitate the dog and find great homes. We help small dogs find forever homes.",
                    URL = "http://www.chirescuetruckeemeadows.com/",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_2_9bd5c1a6-78a4-4579-8542-af070603d6d1_Ch_Resuce_TM_logo_22.jpg",
                    Email = "ChiRescue@ymail.com",
                    PhoneNumber = "775-825-2412",
                    EventDate = DateTime.Now,
                    EventHours = "",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Event").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "Petco on South Virginia, Reno, NV").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "DLSmith").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Paw Impressions",
                    Description = "Free Event! - Create a memory that lasts a lifetime. Bring your dog in to create an imprint of their paw, and it�s yours to cherish. We'll provide all the supplies you need.",
                    URL = "",
                    PhotoPath = "",
                    Email = "",
                    PhoneNumber = "",
                    EventDate = new DateTime(2015, 10, 10, 10, 0, 0),
                    EventHours = "10:00AM - 03:00PM",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Event").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "Petco on South Virginia, Reno, NV").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "DLSmith").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Adoptable Pets Meet & Greet with NEVADA HUMANE SOCIETY",
                    Description = "Connecting adoptable pets with loving parents is our passion. You�re invited to stop by and meet a few of our favorites who are ready for happy homes. With our Welcome to the Family program, you can streamline the process and get exclusive savings.",
                    URL = "",
                    PhotoPath = "",
                    Email = "",
                    PhoneNumber = "",
                    EventDate = new DateTime(2015, 10, 17, 11, 0, 0),
                    EventHours = "11:00AM - 04:00PM",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Event").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "Petco on Northtowne, Reno, NV").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "DLSmith").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Adoptable Pets Meet & Greet with NEVADA HUMANE SOCIETY at Petco North",
                    Description = "Connecting adoptable pets with loving parents is our passion. You�re invited to stop by and meet a few of our favorites who are ready for happy homes. With our Welcome to the Family program, you can streamline the process and get exclusive savings.",
                    URL = "",
                    PhotoPath = "",
                    Email = "",
                    PhoneNumber = "",
                    EventDate = new DateTime(2015, 10, 17, 11, 0, 0),
                    EventHours = "11:00AM - 03:00PM",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Event").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "Petco on Northtowne, Reno, NV").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "DLSmith").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = false,
                    OnlyCreatorCanAddBoardPhotos = false,
                    Name = "Adoptable Dogs Meet & Greet with CHIHUAHUA RESCUE TRUCKEE MEADOWS, INC",
                    Description = "Connecting adoptable pups with loving parents is our passion. You�re invited to stop by and meet a few of our favorites who are ready for happy homes. With our Welcome to the Family program, you can streamline the process and get exclusive savings.",
                    URL = "",
                    PhotoPath = "",
                    Email = "",
                    PhoneNumber = "",
                    EventDate = new DateTime(2015, 10, 25, 11, 0, 0),
                    EventHours = "11:00AM - 03:00PM",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 0,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Photo").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "None").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = true,
                    OnlyCreatorCanAddBoardPhotos = true,
                    Name = "RonMan's Dogs",
                    Description = "View my large family of rescued pooches.",
                    URL = "",
                    PhotoPath = "",
                    Email = "",
                    PhoneNumber = "",
                    EventDate = DateTime.Now,
                    EventHours = "",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 16,
                    ReviewCount = 0,
                    ReviewAverage = 0m
                },
                new Board {
                    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "Photo").Id,
                    LocationId = locations.FirstOrDefault(l => l.Name == "None").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    OnlyCreatorCanUpdateMainBoardInfo = true,
                    OnlyCreatorCanAddBoardPhotos = true,
                    Name = "All in the Family",
                    Description = "All in the Family",
                    URL = "",
                    PhotoPath = "",
                    Email = "",
                    PhoneNumber = "",
                    EventDate = DateTime.Now,
                    EventHours = "",
                    EventSponsors = "",
                    VolunteerOpportunities = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    BoardFlagCount = 0,
                    CommentCount = 0,
                    PhotoCount = 20,
                    ReviewCount = 0,
                    ReviewAverage = 0m

                //},
                //new Board {
                //    BoardTypeId = boardTypes.FirstOrDefault(bt => bt.Name == "").Id,
                //    LocationId = locations.FirstOrDefault(l => l.Name == "").Id,
                //    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                //    OnlyCreatorCanUpdateMainBoardInfo = false,
                //    OnlyCreatorCanAddBoardPhotos = false,
                //    Name = "",
                //    Description = "",
                //    URL = "",
                //    PhotoPath = "",
                //    Email = "",
                //    PhoneNumber = "",
                //    EventDate = DateTime.Now,
                //    EventHours = "",
                //    EventSponsors = "",
                //    VolunteerOpportunities = "",
                //    CreatedOn = DateTime.Now,
                //    UpdatedOn = DateTime.Now,
                //    Inactive = false,
                //    BoardFlagCount = 0,
                //    CommentCount = 0,
                //    PhotoCount = 0,
                //    ReviewCount = 0,
                //    ReviewAverage = 0m

                }
            };

            context.Boards.AddOrUpdate(b => b.Name, boards);
            context.SaveChanges();

            var photos = new Photo[] {
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Serena",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_1c76bfce-2947-487c-ab53-529c52db5031_serena_1.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Buttercup",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_83a4b913-7dd2-4e6c-9afe-36f2509531b3_buttercup_1.png",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Daisy",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_24274a73-cf73-4e84-81a0-2565448d2b67_daisy_1.png",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "GidgetWidget",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_1269a1d7-8fc9-4131-aecb-f43e765cf459_gidgetwidget0.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Jasper",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_ecc8eedf-2696-4c13-a1ff-31cee5eb9968_jasper-1.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Motley",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_d1a93440-88dc-45e3-9b77-a97f1f0621c3_motley_1.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Nora",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_9a783e80-2f0a-476f-adec-e4285f1d60bc_nora1.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Pearl",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_83e82bf0-d4f0-4149-90e4-198cd60ff830_pearl_1.png",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Penny",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_cc298976-cd44-4815-a140-ed6a71d81b9b_penny.png",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Petey",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_a1d481a3-1e70-4b08-9bc5-ff49947c40c2_petey1.png",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Rusty",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_bca49810-2ab7-4b8f-8392-2339a0b73489_rusty_1.png",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Timmy",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_2457a98d-d721-45d7-bc4b-2685ec69dc7b_timmy1.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Trixie with a bone",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_8e367701-2111-450f-b2fc-283067127788_trixie_bone.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Trixie set",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_0e7e72eb-4637-4f11-94bf-1359caf94305_trixie_set.png",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Willow",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_ca168af1-48f4-4ccb-a965-4408f0933380_willow_1.png",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "RonMan's Dogs").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Willow with kids",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/1_396f55a6-812c-4b9e-a71b-6089325d58ab_willow_pics.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },

                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_01",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_d748c87e-1241-4fda-8944-3dfee0b9422c_Family_1.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_02",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_f89ea34a-5213-4057-83f2-e35c53f2f175_Family_2.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_03",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_bc28c3c6-0cde-4a62-aaf9-adf4a7b1ef59_Family_3.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_04",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_74f91121-5b7d-4fa7-a5ac-800c021fc69d_Family_4.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_05",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_8d1d3856-f188-49eb-8d17-fe74f7287913_Family_5.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_06",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_4dcf4957-8036-41b8-baa1-f0fc933ad344_Family_6.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_07",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_8e4e6684-b458-453b-be97-283553e1970d_Family_7.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_08",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_db557569-2923-4c8d-8aff-5c4ef844e381_Family_8.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_09",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_7a25430e-99fa-4b87-89e3-a466cef9daad_Family_9.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_10",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_bc87b443-53a6-430a-9225-094f46b0df70_Family_10.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_11",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_c560c90f-3df5-4d3e-9305-05b082e9be25_Family_11.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_12",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_3e71d772-b6dc-4eea-bf74-7a2212c71e1d_Family_12.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },

                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_13",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_c313db0f-0bd2-4b63-9b8a-347f7191fa8b_Family_13.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_14",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_a5731305-5383-4277-9d3f-edc05683969c_Family_14.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_15",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_19276eb5-a778-4529-9017-5faf9a329aeb_Family_15.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_16",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_9c3ff6d3-4e1b-4e3e-90e6-2a30b901b77d_Family_16.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_17",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_d995541d-7d8f-4e95-b5fb-a1c8dfda36f0_Family_17.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_18",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_d8e554cd-9a8c-480c-a6cc-c88ea8e39a10_Family_18.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_19",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_2b2f00fa-9c89-496f-bab0-852892258479_Family_19.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                },
                new Photo {
                    BoardId = boards.FirstOrDefault(b => b.Name == "All in the Family").Id,
                    UserProfileId = userProfiles.FirstOrDefault(up => up.ScreenName == "RonMan").Id,
                    Description = "Family_20",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/99_1_a0cc5e08-6786-4aa8-80ef-333781719f15_Family_10.jpg",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                    PhotoFlagCount = 0
                }
            };

            context.Photos.AddOrUpdate(p => p.PhotoPath, photos);
            context.SaveChanges();

            var vendors = new Vendor[] {
                new Vendor {
                    Name = "Petco on Northtowne, Reno, NV",
                    Email = "",
                    PhoneNumber = "775-673-9200",
                    ContactName = "Jane Doe",
                    AddressLine1 = "2970 Northtowne Ln",
                    AddressLine2 = "",
                    City = "Reno",
                    StateCode = "NV",
                    PostalCode = "89512",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                },
                new Vendor {
                    Name = "Chihuahua Rescue Truckee Meadows, Reno, NV",
                    Email = "ChiRescue@ymail.com",
                    PhoneNumber = "775-825-2412",
                    ContactName = "Mary Smith",
                    AddressLine1 = "5303 Louie Ln",
                    AddressLine2 = "",
                    City = "Reno",
                    StateCode = "NV",
                    PostalCode = "89511",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Inactive = false,
                }
            };

            context.Vendors.AddOrUpdate(v => v.Name, vendors);
            context.SaveChanges();

            var ads = new Ad[] {
                new Ad {
                    VendorId = vendors.FirstOrDefault(v => v.Name == "Petco on Northtowne, Reno, NV").Id,
                    Name = "Petco 50th Anniversary Sale",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/A_1_af73edde-d65f-4ddc-8246-b2fb4b36f106_Petco_ShoppingPass_500x500.jpg",
                    URL = "http://www.petco.com/petco_Page_PC_octoberanny2015.aspx",
                    StartDate = new DateTime(2015, 10, 1),
                    EndDate =  new DateTime(2015, 11, 1),
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    ClickCount = 0
                },
                 new Ad {
                    VendorId = vendors.FirstOrDefault(v => v.Name == "Chihuahua Rescue Truckee Meadows, Reno, NV").Id,
                    Name = "Donate to Chihuahua Rescue Truckee Meadows by purchasing at Amazon.com",
                    PhotoPath = "https://rondanishurl.blob.core.windows.net/roverslistimages/A_9_7ded8179-2443-47ac-9ee5-6586adc217dd_AmazonSmile_ChihuahuaRescue.jpg",
                    URL = "http://smile.amazon.com/ch/45-2380152",
                    StartDate = new DateTime(2015, 10, 1),
                    EndDate =  new DateTime(2016, 1, 1),
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    ClickCount = 0
                }
            };

            context.Ads.AddOrUpdate(a => a.Name, ads);
            context.SaveChanges();
        }
    }
}
