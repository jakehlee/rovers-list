﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.DTO
{
    public class BoardDetailDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int BoardTypeId { get; set; }
        public string BoardTypeName { get; set; }

        public int UserProfileId { get; set; }
        public string ScreenName { get; set; }

        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string PostalCode { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public bool OnlyCreatorCanUpdateMainBoardInfo { get; set; }
        public bool OnlyCreatorCanAddBoardPhotos { get; set; }

        public string URL { get; set; }
        public string PhotoPath { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public DateTime EventDate { get; set; }
        public string EventHours { get; set; }
        public string EventSponsors { get; set; }
        public string VolunteerOpportunities { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool Inactive { get; set; }

        public int BoardFlagCount { get; set; }
        public int CommentCount { get; set; }
        public int PhotoCount { get; set; }
        public int ReviewCount { get; set; }
        public decimal ReviewAverage { get; set; }

        public IEnumerable<CommentDTO> Comments { get; set; }
        public IEnumerable<PhotoDTO> Photos { get; set; }
        public IEnumerable<ReviewDTO> Reviews { get; set; }

    }
}