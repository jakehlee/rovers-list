﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.DTO
{
    public class PhotoDTO
    {
        public int Id { get; set; }
        public string PhotoPath { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool Inactive { get; set; }
        public int PhotoFlagCount { get; set; }
        public int BoardId { get; set; }
        public int UserProfileId { get; set; }
        public string ScreenName { get; set; }

    }
}