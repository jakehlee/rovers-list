﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.DTO
{
    public class CommentDTO
    {
        public int Id { get; set; }
        public string CommentText { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Inactive { get; set; }
        public int CommentFlagCount { get; set; }
        public int BoardId { get; set; }
        public int UserProfileId { get; set; }
        public string ScreenName { get; set; }

    }
}