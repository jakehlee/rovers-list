﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.DTO
{
    public class PhotoAPI
    {
        public int Id { get; set; }
        public int BoardId { get; set; }
        public int UserProfileId { get; set; }

        public string PhotoPath { get; set; }
        public string Description { get; set; }

        public bool Inactive { get; set; }

    }
}