﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.DTO
{
    public class BoardAPI
    {
        public int Id { get; set; }
        public int BoardTypeId { get; set; }
        public int LocationId { get; set; }
        public int UserProfileId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool OnlyCreatorCanUpdateMainBoardInfo { get; set; }
        public bool OnlyCreatorCanAddBoardPhotos { get; set; }

        public string URL { get; set; }
        public string PhotoPath { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public DateTime EventDate { get; set; }
        public string EventHours { get; set; }
        public string EventSponsors { get; set; }
        public string VolunteerOpportunities { get; set; }

        public bool Inactive { get; set; }

    }
}