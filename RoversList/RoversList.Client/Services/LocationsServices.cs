﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class LocationsService : ILocationsService {

        private IGenericRepository _repo;

        public LocationsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<Location> ListLocations() {
            return _repo.Query<Location>().ToList();
        }

        public Location FindLocation(int id) {
            return _repo.Query<Location>().FirstOrDefault(v => v.Id == id);
        }

        public Location CreateLocation(Location location) {
            location.CreatedOn = DateTime.Now;
            location.UpdatedOn = location.CreatedOn;
            _repo.Add<Location>(location);
            _repo.SaveChanges();
            return location;
        }

        public Location EditLocation(Location location) {
            //  get the original and process it

            var original = _repo.Query<Location>().FirstOrDefault(a => a.Id == location.Id);

            original.UpdatedOn = DateTime.Now;

            _repo.SaveChanges();
            return original;
        }

        public void DeleteLocation(int id) {
            _repo.Delete<Location>(id);
            _repo.SaveChanges();

        }

    }
}