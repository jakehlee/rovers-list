﻿using System;
namespace RoversList.Client.Services {
    public interface IUserTagsService {
        RoversList.Client.DomainClasses.UserTag CreateUserTag(RoversList.Client.DomainClasses.UserTag userTag);
        void DeleteUserTag(int id);
        RoversList.Client.DomainClasses.UserTag EditUserTag(RoversList.Client.DomainClasses.UserTag userTag);
        RoversList.Client.DomainClasses.UserTag FindUserTag(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.UserTag> ListUserTags();
    }
}
