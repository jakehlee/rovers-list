﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class VendorsService : IVendorsService {

        private IGenericRepository _repo;

        public VendorsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<Vendor> ListVendors() {
            return _repo.Query<Vendor>().Include(a => a.Ads).ToList();
        }

        public Vendor FindVendor(int id) {
            return _repo.Query<Vendor>().Include(a => a.Ads).FirstOrDefault(v => v.Id == id);
        }

        public Vendor CreateVendor(Vendor vendor) {
            vendor.CreatedOn = DateTime.Now;
            vendor.UpdatedOn = vendor.CreatedOn;
            _repo.Add<Vendor>(vendor);
            _repo.SaveChanges();
            return vendor;
        }

        public Vendor EditVendor(Vendor vendor) {
            //  get the original and process it

            var original = _repo.Query<Vendor>().FirstOrDefault(a => a.Id == vendor.Id);

            original.UpdatedOn = DateTime.Now;

            _repo.SaveChanges();
            return original;
        }

        public void DeleteVendor(int id) {
            _repo.Delete<Vendor>(id);
            _repo.SaveChanges();

        }

    }
}