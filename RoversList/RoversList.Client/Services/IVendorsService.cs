﻿using System;
namespace RoversList.Client.Services {
    public interface IVendorsService {
        RoversList.Client.DomainClasses.Vendor CreateVendor(RoversList.Client.DomainClasses.Vendor vendor);
        void DeleteVendor(int id);
        RoversList.Client.DomainClasses.Vendor EditVendor(RoversList.Client.DomainClasses.Vendor vendor);
        RoversList.Client.DomainClasses.Vendor FindVendor(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.Vendor> ListVendors();
    }
}
