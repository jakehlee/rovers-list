﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class ActivityLogsService : IActivityLogsService {

        private IGenericRepository _repo;

        public ActivityLogsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<ActivityLog> ListActivityLogs() {
            return _repo.Query<ActivityLog>().ToList();
        }

        public ActivityLog FindActivityLog(int id) {
            return _repo.Query<ActivityLog>().FirstOrDefault(v => v.Id == id);
        }

        public ActivityLog CreateActivityLog(ActivityLog activityLog) {
            activityLog.CreatedOn = DateTime.Now;

            _repo.Add<ActivityLog>(activityLog);
            _repo.SaveChanges();
            return activityLog;
        }

        public ActivityLog EditActivityLog(ActivityLog activityLog) {
            //  get the original and process it

            var original = _repo.Query<ActivityLog>().FirstOrDefault(a => a.Id == activityLog.Id);

            _repo.SaveChanges();
            return original;
        }

        public void DeleteActivityLog(int id) {
            _repo.Delete<ActivityLog>(id);
            _repo.SaveChanges();

        }

    }
}