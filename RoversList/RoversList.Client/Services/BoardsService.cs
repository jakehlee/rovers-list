﻿using RoversList.Client.DomainClasses;
using RoversList.Client.DTO;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class BoardsService : IBoardsService
    {

        private IGenericRepository _repo;

        public BoardsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IEnumerable<BoardDTO> ListBoards() {
            return _repo.Query<Board>()
            .Select(b => new BoardDTO()
            {
                Id = b.Id,
                Name = b.Name,
                Description = b.Description,
                UpdatedOn = b.UpdatedOn,
                BoardTypeId = b.BoardTypeId,
                BoardTypeName = b.BoardType.Name,
                UserProfileId = b.UserProfileId,
                ScreenName = b.UserProfile.ScreenName,
                LocationId = b.LocationId,
                LocationName = b.Location.Name,
                AddressLine1 = b.Location.AddressLine1,
                AddressLine2 = b.Location.AddressLine2,
                City = b.Location.City,
                StateCode = b.Location.StateCode,
                PostalCode = b.Location.PostalCode,
                Latitude = b.Location.Latitude,
                Longitude = b.Location.Longitude
            }).ToList();
        }

        public BoardDetailDTO FindBoard(int id)
        {
            return _repo.Query<Board>()
                .Include(b => b.BoardType)
                .Include(b => b.UserProfile)
                .Include(b => b.Location)
                .Include(b => b.Comments.Select(c => c.UserProfile))
                .Include(b => b.Photos.Select(p => p.UserProfile))
                .Include(b => b.Reviews.Select(r => r.UserProfile))
                .Select(b => new BoardDetailDTO()
                {
                    Id = b.Id,
                    Name = b.Name,
                    Description = b.Description,

                    BoardTypeId = b.BoardTypeId,
                    BoardTypeName = b.BoardType.Name,

                    UserProfileId = b.UserProfileId,
                    ScreenName = b.UserProfile.ScreenName,

                    LocationId = b.LocationId,
                    LocationName = b.Location.Name,
                    AddressLine1 = b.Location.AddressLine1,
                    AddressLine2 = b.Location.AddressLine2,
                    City = b.Location.City,
                    StateCode = b.Location.StateCode,
                    PostalCode = b.Location.PostalCode,
                    Latitude = b.Location.Latitude,
                    Longitude = b.Location.Longitude,

                    OnlyCreatorCanUpdateMainBoardInfo = b.OnlyCreatorCanUpdateMainBoardInfo,
                    OnlyCreatorCanAddBoardPhotos = b.OnlyCreatorCanAddBoardPhotos,

                    URL = b.URL,
                    PhotoPath = b.PhotoPath,
                    Email = b.Email,
                    PhoneNumber = b.PhoneNumber,

                    EventDate = b.EventDate,
                    EventHours = b.EventHours,
                    EventSponsors = b.EventSponsors,
                    VolunteerOpportunities = b.VolunteerOpportunities,

                    Inactive = b.Inactive,
                    CreatedOn = b.CreatedOn,
                    UpdatedOn = b.UpdatedOn,

                    BoardFlagCount = b.BoardFlagCount,
                    CommentCount = b.CommentCount,
                    PhotoCount = b.PhotoCount,
                    ReviewCount = b.ReviewCount,
                    ReviewAverage = b.ReviewAverage,

                    Comments = b.Comments
                        .Select(c => new CommentDTO()
                        {
                            Id = c.Id,
                            CommentText = c.CommentText,
                            CreatedOn = c.CreatedOn,
                            Inactive = c.Inactive,
                            CommentFlagCount = c.CommentFlagCount,
                            BoardId = c.BoardId,
                            UserProfileId = c.UserProfileId,
                            ScreenName = c.UserProfile.ScreenName
                        }).AsEnumerable(),

                    Photos = b.Photos
                        .Select(p => new PhotoDTO()
                        {
                            Id = p.Id,
                            PhotoPath = p.PhotoPath,
                            Description = p.Description,
                            CreatedOn = p.CreatedOn,
                            UpdatedOn = p.UpdatedOn,
                            Inactive = p.Inactive,
                            PhotoFlagCount = p.PhotoFlagCount,
                            BoardId = p.BoardId,
                            UserProfileId = p.UserProfileId,
                            ScreenName = p.UserProfile.ScreenName
                        }).AsEnumerable(),

                    Reviews = b.Reviews
                        .Select(r => new ReviewDTO()
                        {
                            Id = r.Id,
                            ReviewText = r.ReviewText,
                            Rating = r.Rating,
                            CreatedOn = r.CreatedOn,
                            Inactive = r.Inactive,
                            ReviewFlagCount = r.ReviewFlagCount,
                            BoardId = r.BoardId,
                            UserProfileId = r.UserProfileId,
                            ScreenName = r.UserProfile.ScreenName
                        }).AsEnumerable()

                }).FirstOrDefault(b => b.Id == id);
        }


        public Board CreateBoard(BoardAPI board) {
            var newBoard = new Board();

            newBoard.BoardTypeId = board.BoardTypeId;
            newBoard.UserProfileId = board.UserProfileId;
            newBoard.LocationId = board.LocationId;

            newBoard.Name = board.Name;
            newBoard.Description = board.Description;
            newBoard.OnlyCreatorCanUpdateMainBoardInfo = board.OnlyCreatorCanUpdateMainBoardInfo;
            newBoard.OnlyCreatorCanAddBoardPhotos = board.OnlyCreatorCanAddBoardPhotos;

            newBoard.URL = board.URL;
            newBoard.PhotoPath = board.PhotoPath;
            newBoard.Email = board.Email;
            newBoard.PhoneNumber = board.PhoneNumber;

            newBoard.EventDate = board.EventDate;
            newBoard.EventHours = board.EventHours;
            newBoard.EventSponsors = board.EventSponsors;
            newBoard.VolunteerOpportunities = board.VolunteerOpportunities;

            newBoard.Inactive = board.Inactive;

            newBoard.BoardFlagCount = 0;
            newBoard.CommentCount = 0;
            newBoard.PhotoCount = 0;
            newBoard.ReviewCount = 0;
            newBoard.ReviewAverage = 0m;

            newBoard.CreatedOn = DateTime.Now;
            newBoard.UpdatedOn = newBoard.CreatedOn;

            _repo.Add<Board>(newBoard);
            _repo.SaveChanges();
            return newBoard;
        }

        public Board EditBoard(BoardAPI board) {
            //  get the original and process it

            var original = _repo.Query<Board>().FirstOrDefault(b => b.Id == board.Id);

            original.BoardTypeId = board.BoardTypeId;
            original.UserProfileId = board.UserProfileId;
            original.LocationId = board.LocationId;

            original.Name = board.Name;
            original.Description = board.Description;
            original.OnlyCreatorCanUpdateMainBoardInfo = board.OnlyCreatorCanUpdateMainBoardInfo;
            original.OnlyCreatorCanAddBoardPhotos = board.OnlyCreatorCanAddBoardPhotos;

            original.URL = board.URL;
            original.PhotoPath = board.PhotoPath;
            original.Email = board.Email;
            original.PhoneNumber = board.PhoneNumber;

            original.EventDate = board.EventDate;
            original.EventHours = board.EventHours;
            original.EventSponsors = board.EventSponsors;
            original.VolunteerOpportunities = board.VolunteerOpportunities;

            original.Inactive = board.Inactive;

            original.UpdatedOn = DateTime.Now;

            _repo.SaveChanges();
            return original;
        }

        public void DeleteBoard(int id) {
            _repo.Delete<Board>(id);
            _repo.SaveChanges();

        }

    }
}