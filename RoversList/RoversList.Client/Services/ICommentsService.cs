﻿using System;
namespace RoversList.Client.Services
{
    public interface ICommentsService
    {
        RoversList.Client.DomainClasses.Comment CreateComment(RoversList.Client.DTO.CommentAPI comment);
        void DeleteComment(int id);
        RoversList.Client.DomainClasses.Comment EditComment(RoversList.Client.DTO.CommentAPI comment);
        RoversList.Client.DomainClasses.Comment FindComment(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.Comment> ListComments();
    }
}
