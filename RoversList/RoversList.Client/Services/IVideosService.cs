﻿using System;
namespace RoversList.Client.Services {
    public interface IVideosService {
        RoversList.Client.DomainClasses.Video CreateVideo(RoversList.Client.DomainClasses.Video video);
        void DeleteVideo(int id);
        RoversList.Client.DomainClasses.Video EditVideo(RoversList.Client.DomainClasses.Video video);
        RoversList.Client.DomainClasses.Video FindVideo(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.Video> ListVideos();
    }
}
