﻿using RoversList.Client.DomainClasses;
using System;
namespace RoversList.Client.Services {
    public interface IAdsService {
        Ad CreateAd(RoversList.Client.DomainClasses.Ad ad);
        void DeleteAd(int id);
        Ad EditAd(Ad ad);
        Ad FindAd(int id);
        System.Collections.Generic.IList<Ad> ListAds();
    }
}
