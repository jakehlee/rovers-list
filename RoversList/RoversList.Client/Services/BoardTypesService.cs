﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class BoardTypesService : IBoardTypesService {

        private IGenericRepository _repo;

        public BoardTypesService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<BoardType> ListBoardTypes() {
            return _repo.Query<BoardType>().ToList();
        }

        public BoardType FindBoardType(int id) {
            return _repo.Query<BoardType>().FirstOrDefault(v => v.Id == id);
        }

        public BoardType CreateBoardType(BoardType boardType) {

            _repo.Add<BoardType>(boardType);
            _repo.SaveChanges();
            return boardType;
        }

        public BoardType EditBoardType(BoardType boardType) {
            //  get the original and process it

            var original = _repo.Query<BoardType>().FirstOrDefault(a => a.Id == boardType.Id);

            _repo.SaveChanges();
            return original;
        }

        public void DeleteBoardType(int id) {
            _repo.Delete<BoardType>(id);
            _repo.SaveChanges();

        }

    }
}