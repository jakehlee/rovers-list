﻿using System;
namespace RoversList.Client.Services {
    public interface ILocationsService {
        RoversList.Client.DomainClasses.Location CreateLocation(RoversList.Client.DomainClasses.Location location);
        void DeleteLocation(int id);
        RoversList.Client.DomainClasses.Location EditLocation(RoversList.Client.DomainClasses.Location location);
        RoversList.Client.DomainClasses.Location FindLocation(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.Location> ListLocations();
    }
}
