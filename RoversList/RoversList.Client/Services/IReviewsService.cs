﻿using System;
namespace RoversList.Client.Services
{
    public interface IReviewsService
    {
        RoversList.Client.DomainClasses.Review CreateReview(RoversList.Client.DTO.ReviewAPI review);
        void DeleteReview(int id);
        RoversList.Client.DomainClasses.Review EditReview(RoversList.Client.DTO.ReviewAPI review);
        RoversList.Client.DomainClasses.Review FindReview(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.Review> ListReviews();
    }
}
