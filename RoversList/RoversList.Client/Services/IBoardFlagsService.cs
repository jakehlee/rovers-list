﻿using System;
namespace RoversList.Client.Services {
    public interface IBoardFlagsService {
        RoversList.Client.DomainClasses.BoardFlag CreateBoardFlag(RoversList.Client.DomainClasses.BoardFlag boardFlag);
        void DeleteBoardFlag(int id);
        RoversList.Client.DomainClasses.BoardFlag EditBoardFlag(RoversList.Client.DomainClasses.BoardFlag boardFlag);
        RoversList.Client.DomainClasses.BoardFlag FindBoardFlag(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.BoardFlag> ListBoardFlags();
    }
}
