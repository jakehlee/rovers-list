﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class CommentFlagsService : ICommentFlagsService {

        private IGenericRepository _repo;

        public CommentFlagsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<CommentFlag> ListCommentFlags() {
            return _repo.Query<CommentFlag>().ToList();
        }

        public CommentFlag FindCommentFlags(int id) {
            return _repo.Query<CommentFlag>().FirstOrDefault(a => a.Id == id);
        }

        public CommentFlag CreateCommentFlags(CommentFlag commentFlags) {
            commentFlags.CreatedOn = DateTime.Now;
            _repo.Add<CommentFlag>(commentFlags);
            _repo.SaveChanges();
            return commentFlags;
        }

        public CommentFlag EditCommentFlags(CommentFlag commentFlags) {
            //  get the original and process it

            var original = _repo.Query<CommentFlag>().FirstOrDefault(a => a.Id == commentFlags.Id);

            _repo.SaveChanges();
            return original;
        }

        public void DeleteCommentFlags(int id) {
            _repo.Delete<CommentFlag>(id);
            _repo.SaveChanges();

        }

    }
}