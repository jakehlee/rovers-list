﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class BoardFlagsService : IBoardFlagsService {

        private IGenericRepository _repo;

        public BoardFlagsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<BoardFlag> ListBoardFlags() {
            return _repo.Query<BoardFlag>().ToList();
        }

        public BoardFlag FindBoardFlag(int id) {
            return _repo.Query<BoardFlag>().FirstOrDefault(v => v.Id == id);
        }

        public BoardFlag CreateBoardFlag(BoardFlag boardFlag) {
            boardFlag.CreatedOn = DateTime.Now;

            _repo.Add<BoardFlag>(boardFlag);
            _repo.SaveChanges();
            return boardFlag;
        }

        public BoardFlag EditBoardFlag(BoardFlag boardFlag) {
            //  get the original and process it

            var original = _repo.Query<BoardFlag>().FirstOrDefault(a => a.Id == boardFlag.Id);

            _repo.SaveChanges();
            return original;
        }

        public void DeleteBoardFlag(int id) {
            _repo.Delete<BoardFlag>(id);
            _repo.SaveChanges();

        }

    }
}