﻿using RoversList.Client.DomainClasses;
using System;
using System.Collections.Generic;
namespace RoversList.Client.Services 
{
    public interface IUserProfilesService 
    {
        UserProfile CreateUserProfile(UserProfile userProfile);
        void DeleteUserProfile(int id);
        UserProfile EditUserProfile(UserProfile userProfile);
        UserProfile FindUserProfile(int id);
        UserProfile FindUserProfileByUserId(Guid userId);
        UserProfile FindUserProfileByScreenName(string userId);
        IList<UserProfile> ListUserProfiles();
    }
}
