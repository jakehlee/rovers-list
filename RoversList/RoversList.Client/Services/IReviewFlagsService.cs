﻿using System;
namespace RoversList.Client.Services {
    public interface IReviewFlagsService {
        RoversList.Client.DomainClasses.ReviewFlag CreateReviewFlags(RoversList.Client.DomainClasses.ReviewFlag reviewFlags);
        void DeleteReviewFlags(int id);
        RoversList.Client.DomainClasses.ReviewFlag EditReviewFlags(RoversList.Client.DomainClasses.ReviewFlag reviewFlags);
        RoversList.Client.DomainClasses.ReviewFlag FindReviewFlags(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.ReviewFlag> ListReviewFlags();
    }
}
