﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class PhotoFlagsService : IPhotoFlagsService {

        private IGenericRepository _repo;

        public PhotoFlagsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<PhotoFlag> ListPhotoFlags() {
            return _repo.Query<PhotoFlag>().ToList();
        }

        public PhotoFlag FindPhotoFlags(int id) {
            return _repo.Query<PhotoFlag>().FirstOrDefault(v => v.Id == id);
        }

        public PhotoFlag CreatePhotoFlag(PhotoFlag photoFlags) {
            photoFlags.CreatedOn = DateTime.Now;

            _repo.Add<PhotoFlag>(photoFlags);
            _repo.SaveChanges();
            return photoFlags;
        }

        public PhotoFlag EditPhotoFlag(PhotoFlag photoFlags) {
            //  get the original and process it

            var original = _repo.Query<PhotoFlag>().FirstOrDefault(a => a.Id == photoFlags.Id);

   

            _repo.SaveChanges();
            return original;
        }

        public void DeletePhotoFlag(int id) {
            _repo.Delete<PhotoFlag>(id);
            _repo.SaveChanges();

        }

    }
}