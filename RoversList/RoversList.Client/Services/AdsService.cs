﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class AdsService : IAdsService {

        private IGenericRepository _repo;

        public AdsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<Ad> ListAds() {
            return _repo.Query<Ad>().Include(v => v.Vendor).ToList();
        }

        public Ad FindAd(int id) {
            return _repo.Query<Ad>().Include(v => v.Vendor).FirstOrDefault(a => a.Id == id);
        }

        public Ad CreateAd(Ad ad) {
            ad.CreatedOn = DateTime.Now;
            ad.UpdatedOn = ad.CreatedOn;
            _repo.Add<Ad>(ad);
            _repo.SaveChanges();
            return ad;
        }

        public Ad EditAd(Ad ad) {
            //  get the original and process it

            var original = _repo.Query<Ad>().FirstOrDefault(a => a.Id == ad.Id);

            original.UpdatedOn = DateTime.Now;

            original.Name = ad.Name;
            original.PhotoPath = ad.PhotoPath;
            original.URL = ad.URL;
            original.StartDate = ad.StartDate;
            original.Vendor = ad.Vendor;
            original.VendorId = ad.VendorId;
            original.EndDate = ad.EndDate;
            original.ClickCount = ad.ClickCount;

            _repo.SaveChanges();
            return original;
        }

        public void DeleteAd(int id) {
            _repo.Delete<Ad>(id);
            _repo.SaveChanges();

        }

    }
}