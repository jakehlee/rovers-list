﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services
{
    public class StateService : IStateService
    {
        private IGenericRepository _repo;

        public StateService (IGenericRepository repo)
	    {
            _repo = repo;
	    }

        public IList<State> ListStates()
        {
            return _repo.Query<State>().ToList();
        }
    }
}

