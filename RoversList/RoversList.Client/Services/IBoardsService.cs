﻿using System;
namespace RoversList.Client.Services
{
    public interface IBoardsService
    {
        RoversList.Client.DomainClasses.Board CreateBoard(RoversList.Client.DTO.BoardAPI board);
        void DeleteBoard(int id);
        RoversList.Client.DomainClasses.Board EditBoard(RoversList.Client.DTO.BoardAPI board);
        RoversList.Client.DTO.BoardDetailDTO FindBoard(int id);
        System.Collections.Generic.IEnumerable<RoversList.Client.DTO.BoardDTO> ListBoards();
    }
}
