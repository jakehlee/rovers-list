﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services
{
    public class UserProfilesService : IUserProfilesService
    {

        private IGenericRepository _repo;

        public UserProfilesService(IGenericRepository repo)
        {
            _repo = repo;
        }

        public IList<UserProfile> ListUserProfiles()
        {
            return _repo.Query<UserProfile>().ToList();
        }

        public UserProfile FindUserProfile(int id)
        {
            return _repo.Find<UserProfile>(id);
        }

        public UserProfile FindUserProfileByUserId(Guid userId)
        {
            return _repo.Query<UserProfile>().FirstOrDefault(up => up.UserId == userId.ToString());
        }

        public UserProfile FindUserProfileByScreenName(string screenName)
        {
            return _repo.Query<UserProfile>().FirstOrDefault(up => up.ScreenName == screenName);
        }

        public UserProfile CreateUserProfile(UserProfile userProfile)
        {
            userProfile.CreatedOn = DateTime.Now;
            userProfile.UpdatedOn = userProfile.CreatedOn;
            _repo.Add<UserProfile>(userProfile);
            _repo.SaveChanges();
            return userProfile;
        }

        public UserProfile EditUserProfile(UserProfile userProfile)
        {
            //  get the original and process it

            var original = _repo.Query<UserProfile>().FirstOrDefault(up => up.Id == userProfile.Id);

            original.ScreenName = userProfile.ScreenName;
            original.Description = userProfile.Description;
            original.PhotoPath = userProfile.PhotoPath;
            original.City = userProfile.City;
            original.StateCode = userProfile.StateCode;
            original.Email = userProfile.Email;
            original.DoNotShowEmail = userProfile.DoNotShowEmail;
            original.ContactByEmail = userProfile.ContactByEmail;
            original.CellPhoneNumber = userProfile.CellPhoneNumber;
            original.DoNotShowPhone = userProfile.DoNotShowPhone;
            original.ContactByPhone = userProfile.ContactByPhone;
            original.Inactive = userProfile.Inactive;

            original.UpdatedOn = DateTime.Now;

            _repo.SaveChanges();
            return original;
        }

        public void DeleteUserProfile(int id)
        {
            _repo.Delete<UserProfile>(id);
            _repo.SaveChanges();

        }

    }
}