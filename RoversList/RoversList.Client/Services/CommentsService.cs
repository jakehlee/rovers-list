﻿using RoversList.Client.DomainClasses;
using RoversList.Client.DTO;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class CommentsService : ICommentsService
    {

        private IGenericRepository _repo;

        public CommentsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<Comment> ListComments() {
            return _repo.Query<Comment>().ToList();
        }

        public Comment FindComment(int id) {
            return _repo.Query<Comment>().FirstOrDefault(a => a.Id == id);
        }

        public Comment CreateComment(CommentAPI comment) {
            var newComment = new Comment();

            newComment.BoardId = comment.BoardId;
            newComment.UserProfileId = comment.UserProfileId;
            newComment.CommentText = comment.CommentText;

            newComment.Inactive = false;
            newComment.CommentFlagCount = 0;
            newComment.CreatedOn = DateTime.Now;

            _repo.Add<Comment>(newComment);
            _repo.SaveChanges();
            return newComment;
        }

        public Comment EditComment(CommentAPI comment) {
            //  get the original and process it
            var original = _repo.Query<Comment>().FirstOrDefault(a => a.Id == comment.Id);
            
            // Currently, there is no plan to allow editing of Comments.
            original.CommentText = comment.CommentText;

            // Currently, Comment does not have properties Inactive and UpdatedOn.
            //original.Inactive = comment.Inactive; 
            //original.UpdatedOn = DateTime.Now;


            _repo.SaveChanges();
            return original;
        }

        public void DeleteComment(int id) {
            _repo.Delete<Comment>(id);
            _repo.SaveChanges();

        }

    }
}