﻿using RoversList.Client.DomainClasses;
using RoversList.Client.DTO;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class PhotosService : IPhotosService
    {

        private IGenericRepository _repo;

        public PhotosService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<Photo> ListPhotos() {
            return _repo.Query<Photo>().ToList();
        }

        public Photo FindPhoto(int id) {
            return _repo.Query<Photo>().FirstOrDefault(v => v.Id == id);
        }

        public Photo CreatePhoto(PhotoAPI photo) {
            var newPhoto = new Photo();

            newPhoto.BoardId = photo.BoardId;
            newPhoto.UserProfileId = photo.UserProfileId;

            newPhoto.PhotoPath = photo.PhotoPath;
            newPhoto.Description = photo.Description;

            newPhoto.Inactive = photo.Inactive;

            newPhoto.PhotoFlagCount = 0;
            newPhoto.CreatedOn = DateTime.Now;
            newPhoto.UpdatedOn = newPhoto.CreatedOn;

            _repo.Add<Photo>(newPhoto);
            _repo.SaveChanges();
            return newPhoto;
        }

        public Photo EditPhoto(PhotoAPI photo) {
            //  get the original and process it

            var original = _repo.Query<Photo>().FirstOrDefault(a => a.Id == photo.Id);

            original.PhotoPath = photo.PhotoPath;
            original.Description = photo.Description;
            original.Inactive = photo.Inactive;

            original.UpdatedOn = DateTime.Now;

            _repo.SaveChanges();
            return original;
        }

        public void DeletePhoto(int id) {
            _repo.Delete<Photo>(id);
            _repo.SaveChanges();

        }

    }
}