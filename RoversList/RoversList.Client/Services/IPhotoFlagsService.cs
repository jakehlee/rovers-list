﻿using System;
namespace RoversList.Client.Services {
    public interface IPhotoFlagsService {
        RoversList.Client.DomainClasses.PhotoFlag CreatePhotoFlag(RoversList.Client.DomainClasses.PhotoFlag photoFlags);
        void DeletePhotoFlag(int id);
        RoversList.Client.DomainClasses.PhotoFlag EditPhotoFlag(RoversList.Client.DomainClasses.PhotoFlag photoFlags);
        RoversList.Client.DomainClasses.PhotoFlag FindPhotoFlags(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.PhotoFlag> ListPhotoFlags();
    }
}
