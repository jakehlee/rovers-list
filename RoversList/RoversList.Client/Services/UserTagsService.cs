﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class UserTagsService : IUserTagsService {

        private IGenericRepository _repo;

        public UserTagsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<UserTag> ListUserTags() {
            return _repo.Query<UserTag>().ToList();
        }

        public UserTag FindUserTag(int id) {
            return _repo.Query<UserTag>().FirstOrDefault(v => v.Id == id);
        }

        public UserTag CreateUserTag(UserTag userTag) {
            userTag.CreatedOn = DateTime.Now;

            _repo.Add<UserTag>(userTag);
            _repo.SaveChanges();
            return userTag;
        }

        public UserTag EditUserTag(UserTag userTag) {
            //  get the original and process it

            var original = _repo.Query<UserTag>().FirstOrDefault(a => a.Id == userTag.Id);

            _repo.SaveChanges();
            return original;
        }

        public void DeleteUserTag(int id) {
            _repo.Delete<UserTag>(id);
            _repo.SaveChanges();

        }

    }
}