﻿using System;
namespace RoversList.Client.Services {
    public interface ICommentFlagsService {
        RoversList.Client.DomainClasses.CommentFlag CreateCommentFlags(RoversList.Client.DomainClasses.CommentFlag commentFlags);
        void DeleteCommentFlags(int id);
        RoversList.Client.DomainClasses.CommentFlag EditCommentFlags(RoversList.Client.DomainClasses.CommentFlag commentFlags);
        RoversList.Client.DomainClasses.CommentFlag FindCommentFlags(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.CommentFlag> ListCommentFlags();
    }
}
