﻿using System;
namespace RoversList.Client.Services {
    public interface IBoardTypesService {
        RoversList.Client.DomainClasses.BoardType CreateBoardType(RoversList.Client.DomainClasses.BoardType boardType);
        void DeleteBoardType(int id);
        RoversList.Client.DomainClasses.BoardType EditBoardType(RoversList.Client.DomainClasses.BoardType boardType);
        RoversList.Client.DomainClasses.BoardType FindBoardType(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.BoardType> ListBoardTypes();
    }
}
