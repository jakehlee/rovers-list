﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class ReviewFlagsService : IReviewFlagsService {

        private IGenericRepository _repo;

        public ReviewFlagsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<ReviewFlag> ListReviewFlags() {
            return _repo.Query<ReviewFlag>().ToList();
        }

        public ReviewFlag FindReviewFlags(int id) {
            return _repo.Query<ReviewFlag>().FirstOrDefault(v => v.Id == id);
        }

        public ReviewFlag CreateReviewFlags(ReviewFlag reviewFlags) {
            reviewFlags.CreatedOn = DateTime.Now;

            _repo.Add<ReviewFlag>(reviewFlags);
            _repo.SaveChanges();
            return reviewFlags;
        }

        public ReviewFlag EditReviewFlags(ReviewFlag reviewFlags) {
            //  get the original and process it

            var original = _repo.Query<ReviewFlag>().FirstOrDefault(a => a.Id == reviewFlags.Id);

            _repo.SaveChanges();
            return original;
        }

        public void DeleteReviewFlags(int id) {
            _repo.Delete<ReviewFlag>(id);
            _repo.SaveChanges();

        }

    }
}