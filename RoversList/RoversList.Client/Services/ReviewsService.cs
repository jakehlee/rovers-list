﻿using RoversList.Client.DomainClasses;
using RoversList.Client.DTO;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class ReviewsService : IReviewsService
    {

        private IGenericRepository _repo;

        public ReviewsService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<Review> ListReviews() {
            return _repo.Query<Review>().ToList();
        }

        public Review FindReview(int id) {
            return _repo.Query<Review>().FirstOrDefault(v => v.Id == id);
        }

        public Review CreateReview(ReviewAPI review) {
            var newReview = new Review();

            newReview.BoardId = review.BoardId;
            newReview.UserProfileId = review.UserProfileId;

            newReview.ReviewText = review.ReviewText;
            newReview.Rating = review.Rating;

            newReview.Inactive = false;
            newReview.ReviewFlagCount = 0;
            newReview.CreatedOn = DateTime.Now;

            _repo.Add<Review>(newReview);
            _repo.SaveChanges();
            return newReview;
        }

        public Review EditReview(ReviewAPI review) {
            //  get the original and process it
            var original = _repo.Query<Review>().FirstOrDefault(a => a.Id == review.Id);

            // Currently, there is no plan to allow editing of Reviews.
            original.ReviewText = review.ReviewText;
            original.Rating = review.Rating;

            // Currently, Review does not have properties Inactive and UpdatedOn.
            //original.Inactive = review.Inactive; 
            //original.UpdatedOn = DateTime.Now;

            _repo.SaveChanges();
            return original;
        }

        public void DeleteReview(int id) {
            _repo.Delete<Review>(id);
            _repo.SaveChanges();

        }

    }
}