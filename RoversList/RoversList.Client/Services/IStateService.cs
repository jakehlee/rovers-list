﻿using System;
namespace RoversList.Client.Services
{
    public interface IStateService
    {
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.State> ListStates();
    }
}
