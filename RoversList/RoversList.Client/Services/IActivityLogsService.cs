﻿using System;
namespace RoversList.Client.Services {
    public interface IActivityLogsService {
        RoversList.Client.DomainClasses.ActivityLog CreateActivityLog(RoversList.Client.DomainClasses.ActivityLog activityLog);
        void DeleteActivityLog(int id);
        RoversList.Client.DomainClasses.ActivityLog EditActivityLog(RoversList.Client.DomainClasses.ActivityLog activityLog);
        RoversList.Client.DomainClasses.ActivityLog FindActivityLog(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.ActivityLog> ListActivityLogs();
    }
}
