﻿using System;
namespace RoversList.Client.Services
{
    public interface IPhotosService
    {
        RoversList.Client.DomainClasses.Photo CreatePhoto(RoversList.Client.DTO.PhotoAPI photo);
        void DeletePhoto(int id);
        RoversList.Client.DomainClasses.Photo EditPhoto(RoversList.Client.DTO.PhotoAPI photo);
        RoversList.Client.DomainClasses.Photo FindPhoto(int id);
        System.Collections.Generic.IList<RoversList.Client.DomainClasses.Photo> ListPhotos();
    }
}
