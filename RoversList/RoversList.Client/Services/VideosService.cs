﻿using RoversList.Client.DomainClasses;
using RoversList.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoversList.Client.Services {
    public class VideosService : IVideosService {

        private IGenericRepository _repo;

        public VideosService(IGenericRepository repo) {
            _repo = repo;
        }

        public IList<Video> ListVideos() {
            return _repo.Query<Video>().ToList();
        }

        public Video FindVideo(int id) {
            return _repo.Query<Video>().FirstOrDefault(v => v.Id == id);
        }

        public Video CreateVideo(Video video) {
            video.CreatedOn = DateTime.Now;

            _repo.Add<Video>(video);
            _repo.SaveChanges();
            return video;
        }

        public Video EditVideo(Video video) {
            //  get the original and process it

            var original = _repo.Query<Video>().FirstOrDefault(a => a.Id == video.Id);

            _repo.SaveChanges();
            return original;
        }

        public void DeleteVideo(int id) {
            _repo.Delete<Video>(id);
            _repo.SaveChanges();

        }

    }
}